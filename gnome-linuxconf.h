/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat Software, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#ifndef gnome_linuxconf_h
#define gnome_linuxconf_h

#include <gnome.h>
#include <linuxconf/proto.h>
#include <popt.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <limits.h>
#include <dlfcn.h>
#include <ctype.h>

#include "book.h"
#include "dump.h"
#include "mainform.h"
#include "parent.h"
#include "registries.h"
#include "str.h"
#include "util.h"

#define GNOME_LINUXCONF_TYPE		"gnome_linuxconf_type"
#define GNOME_LINUXCONF_TITLE		"gnome_linuxconf_title"
#define GNOME_LINUXCONF_TABLE		"gnome_linuxconf_table"
#define GNOME_LINUXCONF_CURRENT_ROW	"gnome_linuxconf_table_row"
#define GNOME_LINUXCONF_CURRENT_COLUMN	"gnome_linuxconf_table_column"
#define GNOME_LINUXCONF_TABLE_WIDTH	"gnome_linuxconf_table_width"
#define GNOME_LINUXCONF_TABLE_HEIGHT	"gnome_linuxconf_table_height"
#define GNOME_LINUXCONF_MAINFORM	"gnome_linuxconf_mainform"
#define GNOME_LINUXCONF_RADIO_VALUE	"gnome_linuxconf_radio_value"
#define GNOME_LINUXCONF_CHOICE_DEFAULT	"gnome_linuxconf_choice_default"
#define GNOME_LINUXCONF_CHOICE_VALUE	"gnome_linuxconf_choice_value"
#define GNOME_LINUXCONF_CHOICE_TIPS	"gnome_linuxconf_choice_tip"
#define GNOME_LINUXCONF_COMBO_DEFAULT	"gnome_linuxconf_combo_default"
#define GNOME_LINUXCONF_COMBO_TIPS	"gnome_linuxconf_combo_tip"
#define GNOME_LINUXCONF_LIST_DEFAULT	"gnome_linuxconf_list_default"
#define GNOME_LINUXCONF_XPM_DATA	"gnome_linuxconf_xpm_data"
#define GNOME_LINUXCONF_NEEDS_DUMP	"gnome_linuxconf_needs_dump"
#define GNOME_LINUXCONF_WINDOW_TYPE	"gnome_linuxconf_window_type"
#define GNOME_LINUXCONF_SW		"gnome_linuxconf_scrolled_window"

#define GNOME_LINUXCONF_HINT_HFLAGS	"gnome_linuxconf_hint_hflags"
#define GNOME_LINUXCONF_HINT_VFLAGS	"gnome_linuxconf_hint_vflags"
#define GNOME_LINUXCONF_HINT_HPAD	"gnome_linuxconf_hint_hpad"
#define GNOME_LINUXCONF_HINT_VPAD	"gnome_linuxconf_hint_vpad"

#define GNOME_LINUXCONF_CLIP_SIZE	540

#define HANDLER(x) GtkWidget *handle_##x(GList *args); \
GtkWidget *handle_##x(GList *args)
typedef GtkWidget *(command_handle_fn)(GList *args);

#define DUMPER(x) void dump_##x(GtkWidget *dumpee, gpointer data); \
void dump_##x(GtkWidget *dumpee, gpointer data)
typedef void (command_dump_fn)(GtkWidget *, gpointer);

command_handle_fn *command_handler_by_name(const char *name);
command_dump_fn *command_dumper_by_name(const char *name);

/* #define DEBUG */

#endif
