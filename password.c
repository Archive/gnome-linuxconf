/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

DUMPER(password)
{
	const char *text = gtk_entry_get_text(GTK_ENTRY(dumpee));
	const char *cname = get_widget_relative_name(dumpee);
       	char *name, *c;

	g_return_if_fail(text != NULL);
	g_return_if_fail(cname != NULL);

	if((text == NULL) || (strlen(text) == 0)) {
		return;
	}

	name = g_strdup(cname);
	c = strrchr(name, '.');
	if(c != NULL) {
		*c++ = '\0';
#ifdef DEBUG
		printf("dump %s %s %s\n", name, c, text);
#endif
		g_print("dump %s %s %s\n", name, c, text);
	}

	g_free(name);
}

HANDLER(password)
{
	GtkWidget *widget = NULL;
	char *id = (char*)g_list_nth_data(args, 0);
	char *text = (char*)g_list_nth_data(args, 2);

	g_return_val_if_fail(id != NULL, NULL);
	g_return_val_if_fail(text != NULL, NULL);

	widget = gtk_entry_new();
	gtk_entry_set_text(GTK_ENTRY(widget), text);
	gtk_entry_set_visibility(GTK_ENTRY(widget), FALSE);
	parent_insert(NULL, widget);

	return widget;
}
