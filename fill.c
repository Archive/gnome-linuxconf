/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

/* Insert an invisible widgets to fill up columns. */
DUMPER(fill)
{
}

HANDLER(fill)
{
	parent_insert(NULL, NULL);
	return NULL;
}
