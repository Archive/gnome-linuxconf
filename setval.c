/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

HANDLER(setval)
{
        char *form = (char*)g_list_nth_data(args, 0);
        char *id = (char*)g_list_nth_data(args, 1);
        char *value = concat_strings(g_list_next(g_list_next(args)));

	GtkWidget *widget = NULL;

	widget = get_widget_by_name(id);

	if(!GTK_IS_WIDGET(widget)) {
		char *composite_name = g_strdup_printf("%s.%s", form, id);
		widget = get_widget_by_name(composite_name);
		g_free(composite_name);
	}

	if(!GTK_IS_WIDGET(widget)) {
		g_warning("Widget \"%s.%s\" not found.", form, id);
		return NULL;
	}

	/* Labels, which don't actually have names, so this could never work. */
	if(GTK_IS_LABEL(widget)) {
		gtk_label_set_text(GTK_LABEL(widget), value);
	} else
	/* Entry fields. */
	if(GTK_IS_ENTRY(widget)) {
		gtk_entry_set_text(GTK_ENTRY(widget), value);
	} else
	/* Progress meteres (gauges). */
	if(GTK_IS_PROGRESS(widget)) {
		gtk_progress_set_value(GTK_PROGRESS(widget), atof(value));
	} else
	/* Radio buttons, check boxes, toggleable(?) things. */
	if(GTK_IS_TOGGLE_BUTTON(widget)) {
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(widget),
					     atoi(value));
	} else
	{
		char *linuxconf_type = gtk_object_get_data(GTK_OBJECT(widget),
							   GNOME_LINUXCONF_TYPE);
		if(linuxconf_type != NULL) {
			g_warning("Unknown type for widget \"%s.%s\".",
				  form, id);
		} else {
			g_warning("Unknown type for widget \"%s.%s\": \"%s\".",
				  form, id,
				  linuxconf_type);
		}
	}

	return NULL;
}
