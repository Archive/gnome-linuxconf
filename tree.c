/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

static char *path_to_item(GtkCTree *ctree, int n_item)
{
	GtkCTreeRow *row;
	GtkCTreeNode *child, *node;
	char *path = g_strdup("");
	int i = 0, level = 0, delta = 0;

	/* Now walk the list.  Count the number of children at the current
	 * depth between one node and its parent. */
	node = gtk_ctree_node_nth(ctree, n_item);
	for(i = level = 0; (child = gtk_ctree_node_nth(ctree, i)) != NULL; i++){
		row = GTK_CTREE_ROW(child);
		if((child == node) ||
		   (gtk_ctree_is_ancestor(ctree, child, GTK_CTREE_NODE(node)))){
			char *tmp = NULL;
			tmp = g_strdup_printf("%s%d/", path, delta);
			g_free(path);
			path = tmp;
			delta = 0;
			level++;
		}
		if(row->level == level + 1) {
			delta++;
		}
	}
	return path;
}

/* When the user selects an item, send its ID back to LinuxConf. */
static void action_launch_item(GtkCTree *ctree, GList *node, gint column,
			       gpointer user_data)
{
	GtkCTreeNode *child = NULL;
	const char *top;
	char *path;
	int i, row_number = 0;

	/* If the widget doesn't exist yet, skip this. */
	if((GTK_WIDGET(ctree))->window == NULL) {
		return;
	}

	/* Only print the path if this is a terminal node. */
	if((GTK_CTREE_ROW(GTK_CTREE_NODE(node)))->children != NULL) {
		return;
	}

	top = get_widget_relative_name(GTK_WIDGET(ctree));

	/* Disable all windows while we process and Linuxconf processes. */
	window_list_busy_all(NULL, NULL);

	path = NULL;
	for(i = 0; (child = gtk_ctree_node_nth(ctree, i)) != NULL; i++){
		if(child == GTK_CTREE_NODE(node)) {
			path = path_to_item(ctree, row_number = i);
		}
	}
	g_return_if_fail(path != NULL);

#ifdef DEBUG
	printf("action %s %s 1\n", top, path);
#endif
	g_print("action %s %s 1\n", top, path);

	g_free(path);
}

DUMPER(treemenu)
{
	const char *cname = get_widget_relative_name(dumpee);
	char *path, *c;
	int i, j;
	GtkCTreeNode *child;
	GtkCTreeRow *row;

	g_return_if_fail(cname != NULL);
	path = g_strdup(cname);
	c = strrchr(path, '.');
	if(c != NULL) {
		for(i = j = 0; i < (GTK_CLIST(dumpee))->rows; i++) {
			child = gtk_ctree_node_nth(GTK_CTREE(dumpee), i);
			row = GTK_CTREE_ROW(child);
			if(row->expanded) {
				char *menu = path_to_item(GTK_CTREE(dumpee), i);
#ifdef DEBUG
				printf("dump %s t%d %s\n", path, j, menu);
#endif
				g_print("dump %s t%d %s\n", path, j++, menu);
				g_free(menu);
			}
		}
	}
	g_free(path);
}

/* Build a new CTree widget.  The children aren't widgets of their own, so
 * we have to build another stack here for them. */
HANDLER(treemenu)
{
	GtkWidget *widget = NULL, *scroller;

	char *id = (char*)g_list_nth_data(args, 0);
	g_return_val_if_fail(id != NULL, NULL);

	widget = gtk_ctree_new(1, 0);

	gtk_clist_set_selection_mode(GTK_CLIST(widget), GTK_SELECTION_BROWSE);

	gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_MAINFORM,
			    previous_mainform);

	gtk_signal_connect(GTK_OBJECT(widget), "tree-select-row",
			   GTK_SIGNAL_FUNC(action_launch_item),
			   NULL);

	scroller = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scroller),
				       GTK_POLICY_NEVER,
				       GTK_POLICY_AUTOMATIC);
	gtk_container_add(GTK_CONTAINER(scroller), widget);

        gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_VPAD,
                            GINT_TO_POINTER(-1));
        gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_HPAD,
                            GINT_TO_POINTER(-1));

	parent_insert(NULL, scroller);
	tree_push((GtkCTreeNode*)widget);

	return widget;
}

/* Insert a submenu into the current CTree. */
HANDLER(treesub)
{
	GtkCTreeNode *node;

	char *expand_flag = (char*)g_list_nth_data(args, 0);
	char *icon = (char*)g_list_nth_data(args, 1);
	char *title = (char*)g_list_nth_data(args, 2);

	g_return_val_if_fail(expand_flag != NULL, NULL);
	g_return_val_if_fail(icon != NULL, NULL);
	g_return_val_if_fail(title != NULL, NULL);

	/* Unless the current submenu is the root (where previous_widget is
	 * at the top of the stack), add this as a submenu of the current one
	 * and push it down. */
	node = gtk_ctree_insert_node(GTK_CTREE(previous_widget),
				     (tree_peek() == (GtkCTreeNode*)previous_widget) ?
				     NULL : tree_peek(),
				     NULL,
				     &title,
				     0,
				     NULL,
				     NULL,
				     NULL,
				     NULL,
				     FALSE,
				     (expand_flag != NULL) && atoi(expand_flag));
	gtk_ctree_node_set_text(GTK_CTREE(previous_widget),
				node,
				0,
				title);
	tree_push(node);
	return NULL;
}

/* Insert a terminal node. */
HANDLER(treeelem)
{
	GtkCTreeNode *node;

	char *icon = (char*)g_list_nth_data(args, 0);
	char *title = (char*)g_list_nth_data(args, 1);

	g_return_val_if_fail(icon != NULL, NULL);
	g_return_val_if_fail(title != NULL, NULL);

	/* Unless the current submenu is the root (where previous_widget is
	 * at the top of the stack), add this as an item in the current menu. */
	node = gtk_ctree_insert_node(GTK_CTREE(previous_widget),
				     (tree_peek() == (GtkCTreeNode*)previous_widget) ?
				     NULL : tree_peek(),
				     NULL,
				     &title,
				     0,
				     NULL,
				     NULL,
				     NULL,
				     NULL,
				     TRUE,
				     FALSE);
	gtk_ctree_node_set_text(GTK_CTREE(previous_widget),
				node,
				0,
				title);
	return NULL;
}
