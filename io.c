/*
 * I/O backend for LinuxConf interfaces
 *
 * Copyright (C) 1998 Red Hat, Inc.
 *
 * Authors: Michael K. Johnson <johnsonm@redhat.com>
 *          Erik Troan (ewt@redhat.com)
 *
 */
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdarg.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include "glib.h"
#include "io.h"

static ios *io;

/* We are carefull to handle \0 in the input string, though I don't know 
   why as we just return a normal C string <shrug> */
char *io_get_line(void) {
    char * chptr;
    int leftOvers, bytesRead;
    int i;

    /* remove the last line returned */
    leftOvers = io->bufUsed - io->lastLength;

    /* memcpy isn't safe for overlapping regions :-( */
    for (i = 0; i < leftOvers; i++) io->buf[i] = io->buf[i + io->lastLength];
    io->bufUsed = leftOvers;
    
    while (!(chptr = memchr(io->buf, '\n', io->bufUsed))) {
	if (io->bufUsed == io->bufAlloced) {
	    io->bufAlloced += 1024;
	    io->buf = realloc(io->buf, io->bufAlloced + 1);
	}

	bytesRead = read(io->in, io->buf + io->bufUsed, 
			 io->bufAlloced - io->bufUsed);
	if (bytesRead == 0) {
	    if (io->bufUsed) {
		/* note we always malloc + 1 so we can do this :-) */
		io->buf[io->bufUsed] = '\0';
		io->lastLength = io->bufUsed + 1;
		return io->buf;
	    }
	    return NULL;		/* out of bytes */
	}

	io->bufUsed += bytesRead;
    }

    *chptr = '\0';
    io->lastLength = (chptr - io->buf) + 1;

    return io->buf;
}

void io_write_data (char *data) {
    write (io->out, data, strlen(data));
}

void io_write_message(char * format, ...) {
    va_list args;
    static char * buf = NULL;
    static int bufLen = 100;
    int uselen;

    if (!buf) buf = malloc(bufLen);

    va_start(args, format);
    while (uselen = vsnprintf(buf, bufLen, format, args),
		uselen < 0 || uselen > bufLen) {
	bufLen += 50;
	buf = realloc(buf, bufLen);
    }
    va_end(args);

    io_write_data(buf);
}

int io_data_ready(void) {
    fd_set readSet;
    int rc;
    struct timeval tv = { 0, 0};

    /* this presumes we won't ever have a partially completed command sitting
       in the pipe */
    if (io->bufUsed > io->lastLength) return 1;

    FD_ZERO(&readSet);
    FD_SET(io->in, &readSet);
    rc = select(io->in + 1, &readSet, NULL, NULL, &tv);

    return (rc > 0);
}

static int min(int a, int b) {
	return a < b ? a : b;
}

static void move_args(int *argcPtr, char ***argvPtr, int start, int numargs) {
	int argc = *argcPtr;
	char ** argv = *argvPtr;
	int i = 0;

	if (start + numargs > argc)
		numargs = argc - start;

	for (i = start; i < argc; i++) {
		argv[i] = argv[min(i + numargs, argc)];
	}

	argc -= numargs;

	*argcPtr = argc;
	*argvPtr = argv;
}

int io_init (ios *this_io, int * argcPtr, char *** argvPtr, preferences pfl) {
	int child;
	int i;
	int argc = *argcPtr;
	char ** argv = *argvPtr;
	char *prefer;

	memset(this_io, 0, sizeof(ios));
	io = this_io;
	io->flags = pfl;

	io->bufAlloced = 100;
	io->buf = malloc(io->bufAlloced + 1);

	/* first look for an alpha or numeric hostname as the FIRST argument
	 * only -- not using --host is a kind of hack...
	 */
	if (argc > 1 && isalnum(argv[1][0])) {
		io->hostname = g_strdup(argv[1]);
		move_args(&argc, &argv, 1, 1);
	}

	/* handle --exec by mangling argv/argc; we want --exec .*$
	 * to just disappear as far as gnome_init knows.
	 * also handle --pipe and --nogfx and keep poor gnome_init()
	 * from having to deal with arguments it's poor head can't
	 * comprehend.  Hopefully that will convince it to call
	 * gtk_init() for us...
	 */
	for (i = 1; i < argc; i++) {
		if (!strcmp(argv[i], "--pipe")) {
			io->in = io->out = atoi(argv[i+1]);
			move_args(&argc, &argv, i, 2);
			i--; /* start over with the same-position argument */
		} else if (!strcmp(argv[i], "--exec")) {
			io->exec_argc = argc - i - 1;
			io->exec_argv = malloc(sizeof(char *) * 
						(io->exec_argc + 1));
			memcpy(io->exec_argv, &argv[i+1], 
				sizeof(char *) * io->exec_argc);
			io->exec_argv[io->exec_argc] = NULL;
			argv[i] = NULL;
			argc = i;
			break;
		} else if (!strcmp(argv[i], "--nogfx")) {
			io->flags |= P_nogfx;
			move_args(&argc, &argv, i, 1);
			i--; /* start over with the same-position argument */
		} else if (!strcmp(argv[i], "--host")) {
			io->hostname = g_strdup(argv[i+1]);
			move_args(&argc, &argv, i, 2);
			i--; /* start over with the same-position argument */
		} else if (!strcmp(argv[i], "--treemenu")) {
			io->flags |= P_treemenu;
			move_args(&argc, &argv, i, 1);
			i--; /* start over with the same-position argument */
		} else if (!strcmp(argv[i], "--notreemenu")) {
			io->flags &= ~P_treemenu;
			move_args(&argc, &argv, i, 1);
			i--; /* start over with the same-position argument */
		}
	}

	*argcPtr = argc;
	*argvPtr = argv;


	if (io->exec_argc != 0) {
		/* remap stdin/out to a pipe to an executed command */
		int pipe_to_child[2];
		int pipe_to_parent[2];

		pipe(pipe_to_child);
		pipe(pipe_to_parent);
		if (!(child = fork())) {
			/* use pipes for standard i/o */
			dup2(pipe_to_child[0], STDIN_FILENO);
			dup2(pipe_to_parent[1], STDOUT_FILENO);
			close(pipe_to_child[0]);
			close(pipe_to_child[1]);
			close(pipe_to_parent[0]);
			close(pipe_to_parent[1]);

			execvp(io->exec_argv[0], io->exec_argv);
			fprintf(stderr, "error in exec: %s\n", 
				io->exec_argv[0]);
			_exit(1);
		}
		/* use pipes for standard i/o */
		close(pipe_to_child[0]);
		close(pipe_to_parent[1]);
		io->out = pipe_to_child[1];
		io->in = pipe_to_parent[0];
	} else if (io->hostname) {
		/* remap stdin/out to a socket */
		struct hostent *host;
		struct sockaddr_in address;
		struct in_addr inaddr;
		int sock;

		if (inet_aton(io->hostname, &inaddr))
			host = gethostbyaddr((char *) &inaddr,
				sizeof(inaddr), AF_INET);
		else
			host = gethostbyname(io->hostname);

		/* was lookup successful? */
		if (!host) {
			herror("error looking up host");
			return 1;
		}

		if ((sock = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
			perror("could not open socket");
			return 0;
		}

		address.sin_family = AF_INET;

		/* official port for linuxconf is 98; make this a
		 * command-line configurable option later if necessary */
		address.sin_port = htons(98);

		/* get the first IP address for the remote site */
		memcpy(&address.sin_addr, host->h_addr_list[0],
			sizeof(address.sin_addr));

		if (connect(sock, (struct sockaddr *) &address,
			    sizeof(address))) {
			perror("could not connect to remote system");
			return 0;
		}

		io->in = sock;
		io->out = sock;

		/* send special URL that sends linuxconf into
		 * remadmin mode
		 */
		io_write_data ("get /remadm: http/1.0\n\n");

	} else if (! io->in) {
		/* just use stdin/stdout */
		io->in = STDIN_FILENO;
		io->out = STDOUT_FILENO;
	}

	prefer = malloc(128); /* more than plenty... */
	strcpy (prefer, "prefer");
	if (io->flags & P_html) strcat(prefer, " html");
	if (io->flags & P_nogfx) strcat(prefer, " nogfx");
	if (io->flags & P_treemenu) strcat(prefer, " treemenu");
	if (io->flags & P_modal) strcat(prefer, " modal");
	io_write_message ("%s\n", prefer);
	free(prefer);

	return 1;
}

