/*
 * A GNOME front end for LinuxConf
 *
 * parse.h: interface to parse.c
 *
 * Copyright (C) 1997 the Free Software Foundation
 * Copyright (C) 1998 Red Hat Software, Inc.
 *
 * Authors: Miguel de Icaza (miguel@gnu.org)
 *          Michael K. Johnson <johnsonm@redhat.com>
 *
 */

#include <glib.h>

#define PROTOCOL_VERSION 3

typedef enum command_type_e {
	C_STR,
	C_NEWLINE,
	C_SKIP,
	C_HLINE,
	C_DISPOLAST,
	C_LABEL,
	C_STRING,
	C_CHECKBOX,
	C_RADIO,
	C_BUTTON_DUMP,
	C_BUTTON,
	C_BUTTON_XPM,
	C_BUTTON_XPMF,
	C_CHOICE,
	C_LIST,
	C_LIST_ITEM,
	C_COMBO,
	C_GROUP,
	C_FORM,
	C_PAGE,
	C_BOOK,
	C_MAINFORM,
	C_DELETE,
	C_DUMP,
	C_ICON_XPM,
	C_XFER_XPM,
	C_SIDETITLE,
	C_FILL,
	C_FORM_BUTTON,
	C_GROUPFIT,
	C_SETWEIGHTLAST,
	C_PASSWORD,
	C_BUTTON_FILL,
	C_CURFIELD,
	C_VERSION,
	C_CLIST,
	C_HTML,
	C_TREEMENU,
	C_TREESUB,
	C_TREEITEM,
	C_LAST
} command_type;





typedef struct command_s command;

/* Hold dispolast information */
typedef enum disp_horiz_e {
	H_LEFT,
	H_CENTER,
	H_RIGHT
} disp_horiz;

typedef enum disp_vert_e {
	V_TOP,
	V_CENTER,
	V_BOTTOM
} disp_vert;

typedef struct disposition_s {
	disp_horiz	horiz;
	disp_vert	vert;
	int		num_horiz;
	int		num_vert;
} disposition;

/* combo items are a GSList of items attached to a C_COMBO */
typedef struct combo_item_s {
	char		*value;
	char		*comment;
} combo_item;

typedef enum choice_e {
	CB_OFF,
	CB_ON
} choice;

/* clist_datas are dropped in the data element, including a pointer to a
 * list of clist_items and a list of titles
 */
typedef struct clist_data_s {
	char **titles;
	GSList *itemlist;
} clist_data;

/* clist items are a GSList of items attached to a C_CLIST */
typedef struct clist_item_s {
	char	*id;
	char	**values;
} clist_item;

/* mainform_data items are attached to the data elements of mainforms */
typedef struct mainform_data_s {
	char	*enteraction;
	command	*focus;
} mainform_data;

/* tree_item_data items are attached to treemenu, treesub,
 * and treeitem elements
 */
typedef struct tree_item_data_s {
	int	num;
	int	numchildren; /* only used for treemenus and treesubs */
	int	expand;      /* only used for treemenus and treesubs */
	char	**icon_xpm;
} tree_item_data;





/* The command type serves two purposes:
 *   o It arranges commands in a tree in the order that the remadmin
 *     protocol refers to them, including which ones should have the
 *     data dumped
 *   o It keeps the commands in a structure that makes it convenient
 *     to dispose of them when we are done with them
 *
 * command contract:
 * All strings are copies that should be freed when the structure
 * is freed.
 * Whenever a child command is created, its path is filled in
 * from the parent, and the parent's child_list is filled in with
 * a pointer to the child command.
 *
 * This structure can be "inherited" by the display engine by
 * creating a structure that is the same as this one, with
 * more elements tacked on at the end.  If a display engine
 * does so, it must provide a command_alloc() to parse_init()
 * in order to allocate the right amount of memory.  If it
 * adds pointer data, it must provide a command_free() which
 * frees those items and then frees the command.
 */
struct command_s {
	command_type	type;
	disposition	*disp;		/* may be NULL */
	char		*id;
	char		*str;		/* may be NULL */
	void		*data;		/* may be NULL */
	int		idata;
	void		*ref;		/* may be NULL */
	char		*path;
	command		*parent;	/* may be NULL */
	command		*form;		/* MAIN form   */
	GSList		*child_list;	/* may be NULL */
};

/* allocate enough memory for a command structure, plus any
 * inheritance space
 */
typedef command* (*command_alloc) (void);

/* delete all the widgets named in the tree rooted at c */
typedef void (*command_delete_tree) (command *c);

/* free any items in extra inheritance space and then free
 * the memory used by the command itself
 */
typedef void (*command_free) (command *c);

/* check protocol version number:
 * - version holds the integer version number
 * - good_so_far is 1 if the parser is happy with the version, 0 if not.
 * returns 1 if front end is happy with the version, 0 if not.
 */
typedef int (*protocol_check) (int version, int good_so_far, char *bad_message);

typedef void (*write_data) (char *data);

typedef void (*dump_tree) (command *c);

typedef struct callbacks_s {
	command_alloc		commandAlloc;
	command_delete_tree	commandDeleteTree;
	command_free		commandFree;
	protocol_check		protocolCheck;
	write_data		writeData;
	dump_tree		dumpTree;
} callbacks;



/* parse_init() takes a callback structure as an argument.
 * Any of the command_* may be NULL.  If commandAlloc is non-NULL,
 * commandFree probably should be too.
 */
int
parse_init(callbacks cb);

/* parse_line() takes 1 char * argument, the current line.
 * if the line is the end of a top-level frame, parse_line() returns
 * a valid command which should be displayed.  Otherwise it returns
 * NULL.
 */
command*
parse_line(char *line);

/* parse_get_tree_path takes a command structure and returns the
 * path to that command as needed for dump and action.
 * The CALLER is responsible for freeing the returned path with g_free()
 */
char*
parse_get_tree_path(command *c);
