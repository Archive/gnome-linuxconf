/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

char *str_accumulator = NULL;

HANDLER(str)
{
	char *text = concat_strings(args);

	g_return_val_if_fail(text != NULL, NULL);
	if(str_accumulator != NULL) {
		char *newacc = g_strconcat(str_accumulator,
					   text, "\n", NULL);
		g_free(str_accumulator);
		g_free(text);
		str_accumulator = newacc;
	} else {
		char *newacc = g_strconcat(text, "\n", NULL);
		g_free(text);
		str_accumulator = newacc;
	}

	return NULL;
}
