/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

DUMPER(iconxpm)
{
}
HANDLER(iconxpm)
{
	GtkWidget *pixmap;

	char *id = (char*)g_list_nth_data(args, 0);

	g_return_val_if_fail(id != NULL, NULL);

	pixmap = get_widget_by_name(id);
	g_return_val_if_fail(GNOME_IS_PIXMAP(pixmap), NULL);

	pixmap = gnome_pixmap_new_from_gnome_pixmap(GNOME_PIXMAP(pixmap));

        gtk_object_set_data(GTK_OBJECT(pixmap), GNOME_LINUXCONF_HINT_HFLAGS,
                            GINT_TO_POINTER(GTK_SHRINK));
        gtk_object_set_data(GTK_OBJECT(pixmap), GNOME_LINUXCONF_HINT_VFLAGS,
                            GINT_TO_POINTER(GTK_SHRINK));
        gtk_object_set_data(GTK_OBJECT(pixmap), GNOME_LINUXCONF_HINT_HPAD,
                            GINT_TO_POINTER(-1));
        gtk_object_set_data(GTK_OBJECT(pixmap), GNOME_LINUXCONF_HINT_VPAD,
                            GINT_TO_POINTER(-1));

	parent_insert(NULL, pixmap);

	return NULL;
}
