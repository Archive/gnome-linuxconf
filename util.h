#ifndef util_h
#define util_h
	
#include "gnome-linuxconf.h"
	
char *concat_strings(GList *strings);
void free_data(GtkWidget *widget, gpointer data);

#endif
