#ifndef parent_h
#define parent_h

void parent_insert(GtkWidget *parent, GtkWidget *child);
void parent_newline(GtkWidget *parent);

#endif
