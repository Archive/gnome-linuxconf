/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

DUMPER(string)
{
	dump_password(dumpee, data);
}

HANDLER(string)
{
	GtkWidget *widget = NULL;
	char *id = (char*)g_list_nth_data(args, 0);
	char *maxlen = (char*)g_list_nth_data(args, 1);
	char *text = (char*)g_list_nth_data(args, 2);

	g_return_val_if_fail(id != NULL, NULL);

	widget = gtk_entry_new();
	gtk_entry_set_text(GTK_ENTRY(widget), text ? text : "");
	gtk_entry_set_max_length(GTK_ENTRY(widget),
		       		 maxlen ? atoi(maxlen) : LINE_MAX);
	parent_insert(NULL, widget);

	return widget;
}
