/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

DUMPER(textarea)
{
	const char *cname = get_widget_relative_name(dumpee);
	char *path, *c;
	/* FIXME: is this right? */
	g_return_if_fail(cname != NULL);
	path = g_strdup(cname);
	c = strrchr(path, '.');
	if(c != NULL) {
		gint i, w = gtk_text_get_length(GTK_TEXT(dumpee));
		char *text = gtk_editable_get_chars(GTK_EDITABLE(dumpee),
			       			    0, w - 1);
		char **lines = g_strsplit(text, "\n", LINE_MAX);
		for(i = 0; (lines != NULL) && (lines[i] != NULL); i++) {
#ifdef DEBUG
			printf("dump %s %s \"%s\"\n", path, c, lines[i]);
#endif
			g_print("dump %s %s \"%s\"\n", path, c, lines[i]);
		}
	}
	g_free(path);
}

HANDLER(textarea)
{
	GtkWidget *widget = NULL;
	int position = 0;
	char *id = (char*)g_list_nth_data(args, 0);
	char *cols = (char*)g_list_nth_data(args, 1);
	char *rows = (char*)g_list_nth_data(args, 2);

	g_return_val_if_fail(id != NULL, NULL);
	g_return_val_if_fail(cols != NULL, NULL);
	g_return_val_if_fail(rows != NULL, NULL);

	widget = gtk_text_new(NULL, NULL);
	if(str_accumulator != NULL) {
		gtk_editable_insert_text(GTK_EDITABLE(widget),
					 str_accumulator,
					 strlen(str_accumulator),
					 &position);
		g_free(str_accumulator);
		str_accumulator = NULL;
	}
	parent_insert(NULL, widget);

	return widget;
}
