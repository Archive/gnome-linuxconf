/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

HANDLER(xferxpm)
{
	GtkWidget *pixmap = NULL;

	char *id = (char*)g_list_nth_data(args, 0);
	char template[] = "/tmp/gnomeXXXXXX";  
	int fd;

	g_return_val_if_fail(id != NULL, NULL);

	/* For some reason, gnome_pixmap_new_from_xpm_d doesn't like what it
	 * gets when I g_strsplit(str_accumulator), so we'll just use a temp
	 * file for the pixmap and nuke it quickly. */
	if(str_accumulator != NULL) {
		fd = mkstemp(template);
		if(fd != -1) {
			write(fd, str_accumulator, strlen(str_accumulator));
			close(fd);
			pixmap = gnome_pixmap_new_from_file(template);
			if(GNOME_IS_PIXMAP(pixmap)) {
				GnomePixmap *pm = GNOME_PIXMAP(pixmap);
				if(pm->pixmap == NULL) {
					g_warning("Error reading pixmap \"%s\"",
						  id);
				}
			}
#if 0
			g_warning("NOT removing temporary file \"%s\".\n",
				  template);
#else
			remove(template);
#endif
		}
		g_free(str_accumulator);
		str_accumulator = NULL;
	}

	return pixmap;
}
