/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

GHashTable *radio_button_groups = NULL;

static void add_radio_button_group(char *id, GSList *group)
{
	if(radio_button_groups == NULL) {
		radio_button_groups = g_hash_table_new(g_str_hash, g_str_equal);
	}
	g_hash_table_insert(radio_button_groups, id, group);
#ifdef DEBUG
	printf("Inserting radio button group \"%s\".\n", id);
#endif
}

static GSList *get_radio_button_group(const char *id)
{
	if(radio_button_groups != NULL) {
		return (GSList*) g_hash_table_lookup(radio_button_groups, id);
	}
	return NULL;
}

DUMPER(radio)
{
	GSList *group = NULL;
	const char *cname = get_widget_relative_name(dumpee);
	char *path, *c;

	g_return_if_fail(cname != NULL);
	group = gtk_radio_button_group(GTK_RADIO_BUTTON(dumpee));

	while((group != NULL) && (group->data != NULL)) {
		path = g_strdup(cname);
		c = strrchr(path, '.');
		if(c != NULL) {
			*c++ = '\0';
		}
		if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(dumpee))) {
			gpointer datum = NULL;
			int num = 0;

			datum = gtk_object_get_data(GTK_OBJECT(dumpee),
				       		    GNOME_LINUXCONF_RADIO_VALUE);
			num = GPOINTER_TO_INT(datum);
#ifdef DEBUG
			printf("dump %s %s %d\n", path, c, num);
#endif
			g_print("dump %s %s %d\n", path, c, num);
		}
		g_free(path);
		group = g_slist_next(group);
	}
}

HANDLER(radio)
{
	GtkWidget *widget;
	GSList *group;

	char *id = (char*)g_list_nth_data(args, 0);
	char *instance = (char*)g_list_nth_data(args, 1);
	char *state = (char*)g_list_nth_data(args, 2);
	char *text = (char*)g_list_nth_data(args, 3);

	g_return_val_if_fail(id != NULL, NULL);
	g_return_val_if_fail(instance != NULL, NULL);
	g_return_val_if_fail(state != NULL, NULL);

	id = g_strdup_printf("%s.%s", get_widget_name(parents_peek()), id);
	group = get_radio_button_group(id);
#ifdef DEBUG
	printf("Group for \"%s\" %sfound.\n", id, group ? "" : "not ");
#endif
	widget = gtk_radio_button_new_with_label(group, text);
	group = gtk_radio_button_group(GTK_RADIO_BUTTON(widget));
	add_radio_button_group(id, group);

	gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_RADIO_VALUE,
			    GINT_TO_POINTER(atoi(instance)));
	gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_VFLAGS,
			    GINT_TO_POINTER(GTK_SHRINK));
	gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_VPAD,
			    GINT_TO_POINTER(-1));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(widget), atoi(state));

	gtk_signal_connect(GTK_OBJECT(widget), "destroy",
			   GTK_SIGNAL_FUNC(free_data), id);

	parent_insert(NULL, widget);
	return widget;
}
