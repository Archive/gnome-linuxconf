/*
 * A GNOME front end for LinuxConf
 *
 * I/O backend
 *
 * Copyright (C) 1998 Red Hat Software, Inc.
 *
 * Author: Michael K. Johnson <johnsonm@redhat.com>
 *
 */
typedef struct ios_s {
	int in;		/* file descriptor */
	int out;	/* file descriptor */
	char **exec_argv;
	int exec_argc;
	char *hostname;
	char * buf;
	int bufAlloced;
	int bufUsed;
	int lastLength;
	int flags;
} ios;

/* OR together these flags to indicate your preferences */
typedef enum preferences_e {
	P_html = 1<<0,
	P_modal = 1<<1,
	P_nogfx = 1<<2,
	P_treemenu = 1<<3
} preferences;

int io_init (ios *this_io, int * argcPtr, char *** argvPtr, preferences pfl);
void io_write_data (char *data);
void io_write_message(char * format, ...);
int io_data_ready(void);

char *io_get_line(void);
