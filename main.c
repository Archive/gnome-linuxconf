/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

/* The number of the pipe to use to talk to LinuxConf. */
int ipipefd = STDIN_FILENO;
int opipefd = STDOUT_FILENO;
int parse_tag = 0;
void *dlhandle = NULL;
int logfd = -1;

struct {
	int id;
	const char *name;
} commands[] = {
	/* Eh. */
	{-1,			"No-op",	},

	/* Implemented. */
	{P_Book,		"Book",		},
	{P_End,			"End",		},
	{P_Form,		"Form",		},
	{P_Formbutton,		"Formbutton",	},
	{P_Group,		"Group",	},
	{P_Groupfit,		"Groupfit",	},
	{P_MainForm,		"MainForm",	},
	{P_Page,		"Page",		},
	{P_Treemenu,		"Treemenu",	},
	{P_Treesub,		"Treesub",	},
	{P_Treeelem,		"Treeelem",	},
	{P_Dispolast,		"Dispolast",	},
	{P_Fill,		"Fill",		},
	{P_Skip,		"Skip",		},
	{P_Button,		"Button",	},
	{P_Button_xpm,		"Button_xpm",	},
	{P_Button_xpmf,		"Button_xpmf",	},
	{P_Buttonfill,		"Buttonfill",	},
	{P_Checkbox,		"Checkbox",	},
	{P_Choice,		"Choice",	},
	{P_Choice_item,		"Choice_item",	},
	{P_Clist,		"Clist",	},
	{P_Clist_item,		"Clist_item",	},
	{P_Combo,		"Combo",	},
	{P_Combo_item,		"Combo_item",	},
	{P_Curfield,		"Curfield",	},
	{P_Delete,		"Delete",	},
	{P_Dump,		"Dump",		},
	{P_Enteraction,		"Enteraction",	},
	{P_Gauge,		"Gauge",	},
	{P_Hline,		"Hline",	},
	{P_Icon_xpm,		"Icon_xpm",	},
	{P_Label,		"Label",	},
	{P_List,		"List",		},
	{P_List_item,		"List_item",	},
	{P_Newline,		"Newline",	},
	{P_Password,		"Password",	},
	{P_Radio,		"Radio",	},
	{P_Richtext,		"Richtext",	},
	{P_Setval,		"Setval",	},
	{P_Setweightlast,	"Setweightlast",},
	{P_Sheet,		"Sheet",	},
	{P_Sheet_item,		"Sheet_item",	},
	{P_Sidetitle,		"Sidetitle",	},
	{P_Slider,		"Slider",	},
	{P_Str,			"Str",		},
	{P_String,		"String",	},
	{P_Textarea,		"Textarea",	},
	{P_Version,		"Version",	},
	{P_Xfer_xpm,		"Xfer_xpm",	},

	{P_Alive,		"Alive",	},
	{P_Setcontext,		"Setcontext",	},

	/* Not yet implemented, or documented for that matter. */
#ifdef P_Defdc
	{P_Defdc,		"Defdc",	},
#endif
#ifdef P_Deffont
	{P_Deffont,		"Deffont",	},
#endif
#ifdef P_Defpen
	{P_Defpen,		"Defpen",	},
#endif
#ifdef P_Defbrush
	{P_Defbrush,		"Defbrush",	},
#endif
#ifdef P_Drawline
	{P_Drawline,		"Drawline",	},
#endif
#ifdef P_Drawrect
	{P_Drawrect,		"Drawrect",	},
#endif
#ifdef P_Drawarc
	{P_Drawarc,		"Drawarc",	},
#endif
#ifdef P_Fillrect
	{P_Fillrect,		"Fillrect",	},
#endif
#ifdef P_Fillarc
	{P_Fillarc,		"Fillarc",	},
#endif
#ifdef P_Menubar
	{P_Menubar,		"Menubar",	},
#endif
#ifdef P_Popupmenu
	{P_Popupmenu,		"Popupmenu",	},
#endif
#ifdef P_Submenu
	{P_Submenu,		"Submenu",	},
#endif
#ifdef P_Menuentry
	{P_Menuentry,		"Menuentry",	},
#endif
#ifdef P_Clear
	{P_Clear,		"Clear",	},
#endif
#ifdef P_Drawclip
	{P_Drawclip,		"Drawclip",	},
#endif
#ifdef P_Setcursor
	{P_Setcursor,		"Setcursor",	},
#endif
#ifdef P_Blit
	{P_Blit,		"Blit",	},
#endif
#ifdef P_Drawtext
	{P_Drawtext,		"Drawtext",	},
#endif
#ifdef P_Refresh
	{P_Refresh,		"Refresh",	},
#endif
};

/* Canonicalize a keyword so that we can use it to get a function's name. */
static void canonicalize_handler(char *handler_name)
{
	char *p, *q;
	q = handler_name;
	for(p = handler_name; *p != '\0'; p++) {
		if(*p == '_') continue;
		*q++ = tolower(*p);
	}
	*q = '\0';
}

/* Return the command number for a given command string. */
static int command_number_by_name(const char *name)
{
	int i;
	for(i = 0; i < sizeof(commands) / sizeof(commands[0]); i++) {
		if(strcasecmp(commands[i].name, name) == 0) {
			return commands[i].id;
			break;
		}
	}
	return -1;
}

/* Return the command string for a given command number. */
static const char *command_name_by_number(int id)
{
	int i;
	for(i = 0; i < sizeof(commands) / sizeof(commands[0]); i++) {
		if(commands[i].id == id) {
			return commands[i].name;
			break;
		}
	}
	return "No-op";
}

/* Get the handler function by its name. */
command_handle_fn *command_handler_by_name(const char *name)
{
	command_handle_fn *ret = NULL;
	char *cname = g_strdup_printf("handle_%s", name);

	canonicalize_handler(cname + strlen("handle_"));
	ret = (command_handle_fn*) dlsym(dlhandle, cname);
	g_free(cname);

	return ret;
}

/* Get the dumper function by its name. */
command_dump_fn *command_dumper_by_name(const char *name)
{
	command_dump_fn *ret = NULL;
	char *cname = g_strdup_printf("dump_%s", name);

	canonicalize_handler(cname + strlen("dump_"));
	ret = (command_dump_fn*) dlsym(dlhandle, cname);
	g_free(cname);

	return ret;
}

/* Get the handler function by its numeric ID. */
static command_handle_fn *command_handler_by_number(int id)
{
	return command_handler_by_name(command_name_by_number(id));
}

/* Parse (with proper quote-handling) a line of input and dispatch it to
 * the proper handler. */
static void parse_line(GScanner *scanner, const char *line)
{
	int len = strlen(line);
	GTokenType token_type;
	GTokenValue token_value;
	GList *args = NULL;
	command_handle_fn *handler = NULL;
	char *command_string = NULL;
	int command_num = -1;

	/* Feed this particular line to the scanner. */
	g_scanner_input_text(scanner, line, len);

	/* Get the first token. */
	g_scanner_get_next_token(scanner);
	token_type = g_scanner_cur_token(scanner);
	token_value = g_scanner_cur_value(scanner);

	/* What is it? */
	switch(token_type) {
		case G_TOKEN_STRING: {
			/* A string.  Either a type or a specific number in
			 * string form. */
			command_string = g_strdup(token_value.v_string);
			/* It's not a name, so it must be a number. */
			if(atoi(command_string) > 0) {
				command_num = atoi(command_string);
				handler = command_handler_by_number(command_num);
				g_free(command_string);
				command_string = g_strdup(command_name_by_number(command_num));
			}
			/* If it's a name, get the handler. */
			if(handler == NULL) {
				handler = command_handler_by_name(command_string);
				command_num = command_number_by_name(command_string);
			}
			break;
		}
		case G_TOKEN_INT: {
			/* A number.  It *must* be a real handler number. */
			command_num = token_value.v_int;
			command_string = g_strdup(command_name_by_number(command_num));
			handler = command_handler_by_number(command_num);
			break;
		}
		default: {
			break;
		}
	}

	/* Aaaargh! */
	if(handler == NULL) {
		g_free(command_string);
		/* Print out the line we got, for debugging purposes. */
		if(token_type == G_TOKEN_STRING) {
			g_warning("parse_line: unknown or unrecognized command \"%s\".\n",
			          token_value.v_string);
		} else {
			g_warning("parse_line: unknown or unrecognized command %ld.\n",
			          token_value.v_int);
		}
		return;
	}

	/* Read the rest of the line. */
	while((token_type != G_TOKEN_EOF) && (token_type != G_TOKEN_NONE)) {
		g_scanner_get_next_token(scanner);
		token_type = g_scanner_cur_token(scanner);
		token_value = g_scanner_cur_value(scanner);

		switch(token_type) {
			/* Concatenate the string onto the argument list. */
			case G_TOKEN_STRING: {
				args = g_list_append(args,
						g_strdup(token_value.v_string));
				break;
			}
			/* Concatenate the number onto the argument list. */
			case G_TOKEN_INT: {
				args = g_list_append(args,
						g_strdup_printf("%ld",
							token_value.v_int));
				break;
			}
			case G_TOKEN_EOF:
			case G_TOKEN_ERROR:
			case G_TOKEN_NONE: {
				break;
			}
			default: {
#ifdef DEBUG
				printf("Token type %d\n", token_type);
				fflush(NULL);
				break;
#endif
			}
		}
	}

#ifdef DEBUG
	if(command_string != NULL) {
		char *argstring;
		/* Print out the line we got, for debugging purposes. */
		argstring = concat_strings(args);
		printf("%s %s\n", command_string, argstring);
		fflush(NULL);
		g_free(argstring);
	}
#endif

	/* Now actually create a widget if we can. */
	if(handler != NULL) {
		GtkWidget *widget = NULL;
		widget = handler(args);
		if(widget != NULL) {
			/* Keep track of what LinuxConf widget type this is. */
			gtk_object_set_data(GTK_OBJECT(widget),
					    GNOME_LINUXCONF_TYPE,
					    command_string);
			/* Be sure to free the string when the widget gets
			 * destroyed at some future time. */
			gtk_signal_connect(GTK_OBJECT(widget), "destroy",
					   GTK_SIGNAL_FUNC(free_data),
					   command_string);
			/* We got the pointer back because we need to register
			 * it, so do so. */
			register_widget(widget, (char*)g_list_nth_data(args,0));
			previous_widget = widget;
		}
	}

	/* Free the list of arguments we built up previously. */
	if(args != NULL) {
		GList *list;
		for(list = args; list != NULL; list = g_list_next(list)) {
			if(list->data) {
				g_free(list->data);
				list->data = NULL;
			}
		}
		g_list_free(args);
	}

	return;
}

/* Break the input stream up into individual lines and hand them off,
 * one by one, to parse_line. */
static void parse_input(gpointer data, gint source, GdkInputCondition condition)
{
	static char input_buffer[LINE_MAX] = "";
	char **argv;
	int ret = 0;

	g_return_if_fail(condition == GDK_INPUT_READ);

	ret = read(source,
		   input_buffer + strlen(input_buffer),
		   sizeof(input_buffer) - 1 - strlen(input_buffer));
	while(ret != -1) {
		char *rest;

		/* Log the data. */
		if(logfd != -1) {
			write(logfd, input_buffer, ret);
			fsync(logfd);
		}

		if(ret == 0) {
			/* Note that we can't read/write any more. */
#ifdef DEBUG
			printf("Input pipe closed, done parsing.\n");
#endif
			gdk_input_remove(parse_tag);
			parse_tag = 0;
			close(logfd);
			/* If this was a real pipe, bye-bye. */
			if(ipipefd == opipefd) {
				gtk_main_quit();
			}
			break;
		}

		/* Terminate the buffer at the last newline, but keep track
		 * of how much data is left over. */
		if(rindex(input_buffer, '\n') != NULL) {
			rest = rindex(input_buffer, '\n');
			*rest++ = '\0';
		} else {
			rest = NULL;
		}

		/* Now split the input buffer into lines and feed them
		 * into parse_line. */
		argv = g_strsplit(input_buffer, "\n", sizeof(input_buffer));
		if(argv != NULL) {
			int i;
			for(i = 0; (argv != NULL) && (argv[i] != NULL); i++) {
#ifdef DEBUG
				printf("Parsing line '%s'.\n", argv[i]);
#endif
				if(strlen(argv[i]) > 0) {
					parse_line((GScanner*)data, argv[i]);
				}
			}
			g_strfreev(argv);
			argv = NULL;
		}

		/* Attempt to preserv any partial lines we've read. */
		if(rest != NULL) {
			rest = g_strdup(rest);
		}
		memset(input_buffer, '\0', sizeof(input_buffer));
		if(rest != NULL) {
			strncpy(input_buffer, rest, sizeof(input_buffer) - 1);
			g_free(rest);
			rest = NULL;
		}

		/* Append new data to partial strings. */
		ret = read(source,
			   input_buffer + strlen(input_buffer),
			   sizeof(input_buffer) - 1 - strlen(input_buffer));
	}

	/* Add some flicker. */
	if(parents_peek() != NULL) {
		gtk_widget_show_all(parents_peek());
	}
}

/* Just print some data to the output pipe. */
static void print_handler(const char *string)
{
	write(opipefd, string, strlen(string));
}

int main(int argc, char **argv)
{
	GScannerConfig config;
	GScanner *scanner;
	char *logfile = NULL;
	struct poptOption options[] = {
		{"pipe", 'p', POPT_ARG_INT, &ipipefd, 0,
		 "file descriptor to read input from (default 0)", "FD"},
		{"log", 'l', POPT_ARG_STRING, &logfile, 0,
		 "file to log data from linuxconf to", "LOGFILE"},
		POPT_AUTOHELP
		{NULL, 0, 0, NULL, 0, NULL, NULL},
	};
	poptContext ctx = NULL;
	char preferences[LINE_MAX] =	"prefer slider gauge reconfdia setval "
					"alive treemenu context ";
	int rc;
#ifdef DEBUG
	putenv ("GTK_MODULES=gle");
#endif
	g_return_val_if_fail((dlhandle = dlopen(NULL, RTLD_NOW)) != NULL, 1);

	/* Try to initialize GNOME. */
	rc = gnome_init_with_popt_table(PACKAGE, VERSION, argc, argv, options,
					0, &ctx);
	if(rc != 0) {
		printf("Couldn't initialize GNOME libraries, quitting.\n");
		exit(1);
	}

	/* Create the GScanner we use to parse input. */
	memset(&config, 0, sizeof(config));
	config.cset_skip_characters = " \t\n";
	config.cset_identifier_first = G_CSET_A_2_Z G_CSET_a_2_z "0123456789"
				       G_CSET_LATINC G_CSET_LATINS
				       "-=+_|()`'~!@#$%^&*<>[]{}.;:/?";
	config.cset_identifier_nth = config.cset_identifier_first;
	config.scan_identifier = TRUE;
	config.scan_identifier_1char = TRUE;
	config.scan_string_dq = TRUE;
	config.numbers_2_int = TRUE;
	config.identifier_2_string = TRUE;
	scanner = g_scanner_new(&config);

	/* Parse the command-line arguments. */
	ctx = poptGetContext(PACKAGE, argc, argv, options, 0);
	if(poptGetNextOpt(ctx) != -1) {
		exit(0);
	}

	/* Open the logfile. */
	if(logfile != NULL) {
		logfd = open(logfile, O_CREAT | O_TRUNC | O_RDWR, 0644);
		g_return_val_if_fail(logfd != -1, 1);
	}

	/* Set the input pipe to non-blocking and start listening. */
	fcntl(ipipefd, F_SETFL, O_NONBLOCK);
	parse_tag = gdk_input_add(ipipefd, GDK_INPUT_READ, parse_input, scanner);

	/* If we redirected our input, make our output go to the same place. */
	if(ipipefd != STDIN_FILENO) {
		opipefd = ipipefd;
	}
	g_set_print_handler(print_handler);

	/* Let the back-end know how we like to do things. */
	g_print("%s\n", preferences);

	/* Let 'er rip. */
	gtk_main();
	return 0;
}
