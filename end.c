/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

/* A widget has all of its children visible now.  So show it. */
HANDLER(end)
{
	/* If we're in a tree, pop the tree, otherwise, pop a widget. */
	if(tree_peek() != NULL) {
		tree_pop();
	} else {
		GtkWidget *parent = parents_pop();
		if(GTK_IS_WIDGET(parent)) {
			gtk_widget_show_all(parent);
		}
	}
	return NULL;
}
