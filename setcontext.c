/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

/* Make the given widget the new parent, temporarily. */
HANDLER(setcontext)
{
	char *name = (char*)g_list_nth_data(args, 0);

	GtkWidget *widget;
	g_return_val_if_fail(name != NULL, NULL);

	widget = GTK_WIDGET(get_widget_by_name(name));
	g_assert(GTK_IS_WIDGET(widget));
	parents_push(widget);

	return NULL;
}
