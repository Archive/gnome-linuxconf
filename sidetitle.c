/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"
#define ROTATED_FONT "-*-helvetica-medium-r-*-*-[1 12 ~12 1]-*-*-*-*-*-*-*"

HANDLER(sidetitle)
{
	char *text = concat_strings(args);
	GtkWidget *label;
	GtkStyle *style;
	static GdkFont *font = NULL;
	int lbearing, rbearing, width, ascent, descent;

	if(font == NULL) font = gdk_font_load(ROTATED_FONT);
	g_return_val_if_fail(font != NULL, NULL);

	label = gtk_label_new(text);
	gtk_signal_connect(GTK_OBJECT(label), "destroy",
			   GTK_SIGNAL_FUNC(free_data), text);

	style = gtk_style_copy(gtk_widget_get_style(label));
	style->font = font;
	gtk_widget_set_style(label, style);

	gdk_string_extents(font, text, &lbearing, &rbearing,
		       	   &width, &ascent, &descent);

	gtk_widget_set_usize(label,
			     ascent - descent + GNOME_PAD_SMALL,
			     width);

	parent_insert(NULL, label);

	return NULL;
}
