/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

/* Dump out the contents of a widget. */
void dump_widget(GtkWidget *dumpee, gpointer data)
{
	command_dump_fn *fn = NULL;
	char *type = NULL;

	type = (char*) gtk_object_get_data(GTK_OBJECT(dumpee),
					   GNOME_LINUXCONF_TYPE);
	if(type != NULL) {
		fn = command_dumper_by_name(type);
		if(fn == NULL) {
			g_warning("Dump: no dumper found for a \"%s\".\n", type);
		} else {
#ifdef DEBUG
			printf("Dumping a \"%s\":\n", type);
#endif
			fn(dumpee, data);
		}
	}

	if(GTK_IS_CONTAINER(dumpee)) {
		gtk_container_foreach(GTK_CONTAINER(dumpee),
				      dump_widget, data);
	}
}

HANDLER(dump)
{
	char *name = g_list_nth_data(args, 0);
	GtkWidget *dumpee;
       	dumpee = get_widget_by_name(name);
	if(dumpee == NULL) {
		g_warning("Dump: widget \"%s\" not found.", name);
	} else {
		dump_widget(dumpee, NULL);
	}
	return NULL;
}
