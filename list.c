/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

DUMPER(list)
{
	char *path = NULL, *c = NULL, *text = NULL;
	GtkCListRow *row = NULL;
	GtkCell *cell = NULL;
	char *cname = get_widget_relative_name(dumpee);

	/* FIXME: is this correct? */
	g_return_if_fail(cname != NULL);

	path = g_strdup(cname);
	c = strrchr(path, '.');
	if(c != NULL) {
		*c++ = '\0';
		row = g_list_nth_data((GTK_CLIST(dumpee))->selection, 0);
		if(row != NULL) {
			cell = row->cell;
			if(cell->type == GTK_CELL_TEXT) {
				text = ((GtkCellText*)cell)->text;
#ifdef DEBUG
				printf("dump %s %s %s\n", path, c, text);
#endif
				g_print("dump %s %s %s\n", path, c, text);
			}
		}
	}
	g_free(path);
}

HANDLER(list)
{
	GtkWidget *widget = NULL;
	char *id = (char*)g_list_nth_data(args, 0);
	char *visible = (char*)g_list_nth_data(args, 1);
	char *text = concat_strings(g_list_next(g_list_next(args)));

	g_return_val_if_fail(id != NULL, NULL);
	g_return_val_if_fail(visible != NULL, NULL);
	g_return_val_if_fail(text != NULL, NULL);

	widget = gtk_clist_new(1);
	gtk_clist_set_selection_mode(GTK_CLIST(widget), GTK_SELECTION_BROWSE);
	gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_LIST_DEFAULT,
			    text);
	gtk_signal_connect(GTK_OBJECT(widget), "destroy",
			   GTK_SIGNAL_FUNC(free_data), text);
	parent_insert(NULL, widget);

	return widget;
}

HANDLER(listitem)
{
	GtkWidget *clist = NULL;
	char *id = (char*)g_list_nth_data(args, 0);
	char *text = concat_strings(g_list_next(args));
	char *def;
	int row;

	g_return_val_if_fail(id != NULL, NULL);
	g_return_val_if_fail(text != NULL, NULL);

	clist = get_widget_by_name(id);
	g_return_val_if_fail(GTK_IS_CLIST(clist), NULL);
	row = gtk_clist_append(GTK_CLIST(clist), &text);
	gtk_signal_connect(GTK_OBJECT(clist), "destroy",
			   GTK_SIGNAL_FUNC(free_data), text);

	def = gtk_object_get_data(GTK_OBJECT(clist),
				  GNOME_LINUXCONF_LIST_DEFAULT);
	if((text != NULL) && (strcmp(text, def) == 0)) {
		gtk_clist_select_row(GTK_CLIST(clist), row, -1);
	}

	return NULL;
}
