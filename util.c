/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

/* Assemble a freshly-allocated list of whitespace-separated strings from a
 * GList*. */
char *concat_strings(GList *strings)
{
	char *ret = NULL;
	int i, j, l = 0;
	GList *list;
	for(list = strings, l = 0;
	    (list != NULL) && (list->data != NULL);
	    list = g_list_next(list)) {
		l += (strlen((char*)list->data) + 1);
		for(j = 0; j < strlen((char*)list->data); j++) {
			if(isspace(((char*)list->data)[j])) {
				l += 2;
				break;
			}
		}
	}

	if(l > 0) {
		ret = g_malloc0(l);
	} else {
		return g_strdup("");
	}

	i = 0;
	for(list = strings;
	    (list != NULL) && (list->data != NULL);
	    list = g_list_next(list)) {
		gboolean quote = FALSE;
		for(j = 0; j < strlen((char*)list->data); j++) {
			if(isspace(((char*)list->data)[j])) {
				quote = TRUE;
				break;
			}
		}
#ifdef DEBUG
		if(quote) {
			//ret[i++] = '"';
		}
#endif
		memcpy(&ret[i], (char*)list->data, strlen((char*)list->data));
		i += strlen((char*)list->data);
#ifdef DEBUG
		if(quote) {
			//ret[i++] = '"';
		}
#endif
		ret[i++] = ' ';
	}

	ret[l - 1] = '\0';
	ret[i - 1] = '\0';
	return ret;
}

void free_data(GtkWidget *widget, gpointer data)
{
	if(data != NULL) {
		g_free(data);
	} else {
		g_warning("free_data() called with NULL pointer");
	}
}
