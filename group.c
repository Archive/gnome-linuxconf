/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

DUMPER(group)
{
}

HANDLER(group)
{
	GtkWidget *widget = NULL;

	widget = gtk_frame_new(NULL);
	gtk_frame_set_shadow_type(GTK_FRAME(widget), GTK_SHADOW_NONE);

	gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_HPAD,
			    GINT_TO_POINTER(-1));
	gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_VPAD,
			    GINT_TO_POINTER(-1));

	parent_insert(NULL, widget);
	parents_push(widget);

	return widget;
}

DUMPER(groupfit)
{
}

HANDLER(groupfit)
{
	return handle_group(args);
}
