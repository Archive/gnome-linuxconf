#ifndef registries_h
#define registries_h

extern GtkWidget *previous_widget;

void unregister_widget(GtkWidget *widget, const char *cid);
const char *get_widget_name(GtkWidget *widget);
const char *get_widget_base_name(GtkWidget *widget);
const char *get_widget_relative_name(GtkWidget *widget);
void register_widget(GtkWidget *widget, const char *cid);
GtkWidget *get_widget_by_name(const char *id);
GtkWidget *mainform_of(GtkWidget *widget);

GtkWidget *parents_push(GtkWidget *new_parent);
GtkWidget *parents_pop(void);
GtkWidget *parents_peek(void);

GtkCTreeNode *tree_push(GtkCTreeNode *new_parent);
GtkCTreeNode *tree_pop(void);
GtkCTreeNode *tree_peek(void);

int window_list_length(void);
void window_list_add(GtkWidget *window, gpointer ignored);
void window_list_remove(GtkWidget *window, gpointer ignored);
void window_list_unbusy_all(GtkWidget *window, gpointer ignored);
void window_list_busy_all(GtkWidget *window, gpointer ignored);

#endif
