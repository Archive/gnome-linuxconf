/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

/* The previous widget created. */
GtkWidget *previous_widget = NULL;

/* A simple globally-accessible "stack" of parent widgets. */
static GList *parents_stack = NULL;

GtkWidget *parents_push(GtkWidget *new_parent)
{
	g_return_val_if_fail(new_parent != NULL, NULL);
	g_return_val_if_fail(GTK_IS_WIDGET(new_parent), NULL);
	parents_stack = g_list_prepend(parents_stack, new_parent);
	return new_parent;
}

GtkWidget *parents_pop()
{
	GtkWidget *ret = GTK_WIDGET(g_list_nth_data(parents_stack, 0));
	g_return_val_if_fail(parents_stack != NULL, NULL);
	parents_stack = g_list_remove(parents_stack, ret);
	return ret;
}

GtkWidget *parents_peek()
{
	return (GtkWidget*) g_list_nth_data(parents_stack, 0);
}

/* A simple globally-accessible "stack" of tree items. */
static GList *tree_stack = NULL;

GtkCTreeNode *tree_push(GtkCTreeNode *new_parent)
{
	g_return_val_if_fail(new_parent != NULL, NULL);
	tree_stack = g_list_prepend(tree_stack, new_parent);
	return new_parent;
}

GtkCTreeNode *tree_pop()
{
	GtkCTreeNode *ret = (GtkCTreeNode*) g_list_nth_data(tree_stack, 0);
	g_return_val_if_fail(tree_stack != NULL, NULL);
	tree_stack = g_list_remove(tree_stack, ret);
	return ret;
}

GtkCTreeNode *tree_peek()
{
	return (GtkCTreeNode*) g_list_nth_data(tree_stack, 0);
}

/* Functions to handle widget accesses by name. */
static GHashTable *widget_registry = NULL;

void unregister_widget(GtkWidget *widget, const char *cid)
{
	GtkWidget *registered;
	registered = g_hash_table_lookup(widget_registry, cid);
	if(registered == widget) {
#ifdef DEBUG1
		printf("Unregistering \"%s\".\n", cid);
#endif
		g_hash_table_remove(widget_registry, cid);
	}
}

GtkWidget *mainform_of(GtkWidget *widget)
{
	GtkWidget *parent;
	char *linuxconf_type;

	g_assert(GTK_IS_WIDGET(widget));

	for(parent = widget; parent != NULL; parent = parent->parent) {
		linuxconf_type = gtk_object_get_data(GTK_OBJECT(parent),
						     GNOME_LINUXCONF_TYPE);
		if(linuxconf_type != NULL) {
			if(strcasecmp(linuxconf_type, "mainform") == 0) {
				break;
			}
		}
	}

	return parent;
}

const char *get_widget_name(GtkWidget *widget)
{
	if(GTK_IS_OBJECT(widget)) {
#ifdef GNOME_LINUXCONF_NAME
		return (const char*)gtk_object_get_data(GTK_OBJECT(widget),
						        GNOME_LINUXCONF_NAME);
#else
		return (const char*)gtk_widget_get_name(widget);
#endif
	} else {
		return NULL;
	}
}

const char *get_widget_base_name(GtkWidget *widget)
{
	const char *name = NULL;
	if(GTK_IS_OBJECT(widget)) {
		name = get_widget_name(widget);
		if(strrchr(name, '.') != NULL) {
			name = strrchr(name, '.') + 1;
		}
	}
	return name;
}

const char *get_widget_relative_name(GtkWidget *widget)
{
	GtkWidget *mainform = NULL;
	int l;

	mainform = mainform_of(widget);
	g_return_val_if_fail(GTK_IS_WIDGET(mainform), "");
	l = strlen(get_widget_name(mainform)) -
	    strlen(get_widget_base_name(mainform));

	return get_widget_name(widget) + l;
}

void register_widget(GtkWidget *widget, const char *cid)
{
	char *id;
       	const char *pid = NULL;

	/* Get the name of the widget's parent on the parent stack.  Often,
	 * the widget has already been pushed onto the stack, so check for
	 * that case and handle it properly.  */
	if(parents_peek() == widget) {
		if(g_list_next(parents_stack) != NULL) {
			pid = get_widget_name(g_list_next(parents_stack)->data);
		}
	} else {
		pid = get_widget_name(parents_peek());
	}

	/* Now compose the name Linuxconf refers to it as. */
	if(pid != NULL) {
		id = g_strdup_printf("%s.%s", pid, cid);
	} else {
		id = g_strdup_printf("%s", cid);
	}

	/* Initialize the widget registry if we need to. */
	if(widget_registry == NULL) {
		widget_registry = g_hash_table_new(g_str_hash, g_str_equal);
	}

#ifdef DEBUG1
	printf("Registering \"%s\".\n", id);
#endif
	/* Insert it, and add a destroy handler to free the widget's name. */
	g_hash_table_insert(widget_registry, id, widget);
#ifdef GNOME_LINUXCONF_NAME
	gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_NAME, id);
#else
	gtk_widget_set_name(widget, id);
#endif
	gtk_signal_connect(GTK_OBJECT(widget), "destroy",
			   GTK_SIGNAL_FUNC(unregister_widget), id);
	gtk_signal_connect(GTK_OBJECT(widget), "destroy",
			   GTK_SIGNAL_FUNC(free_data), id);
	/* Insert the base part of the name, because LinuxConf often uses it
	 * to populate it afterward (if it's a composite of some kind). */
#ifdef DEBUG1
	printf("Registering \"%s\".\n", cid);
#endif
	cid = g_strdup(cid);
	g_hash_table_insert(widget_registry, (char*)cid, widget);
	gtk_signal_connect(GTK_OBJECT(widget), "destroy",
			   GTK_SIGNAL_FUNC(unregister_widget), (char*)cid);
	gtk_signal_connect(GTK_OBJECT(widget), "destroy",
			   GTK_SIGNAL_FUNC(free_data), (char*)cid);
}

GtkWidget *get_widget_by_name(const char *id)
{
	if(widget_registry == NULL) {
		return NULL;
	} else {
		return g_hash_table_lookup(widget_registry, id);
	}
}

/* Manipulate the list of opened windows. */
static GList *window_list = NULL;

void window_list_add(GtkWidget *window, gpointer ignored)
{
	window_list = g_list_prepend(window_list, window);
}

void window_list_remove(GtkWidget *window, gpointer ignored)
{
	window_list = g_list_remove(window_list, window);
}

static GdkCursor *cursor = NULL;

void window_list_unbusy_all(GtkWidget *unused, gpointer ignored)
{
	GList *win = window_list;
	while((win != NULL) && GTK_IS_WINDOW(win->data)) {
		if((GTK_WIDGET(win->data))->window != NULL) {
			gdk_window_set_cursor((GTK_WIDGET(win->data))->window,
				       	      NULL);
		}
		win = g_list_next(win);
	}
}

void window_list_busy_all(GtkWidget *unused, gpointer ignored)
{
	GList *win = window_list;
	while((win != NULL) && GTK_IS_WINDOW(win->data)) {
		if(cursor == NULL) {
			cursor = gdk_cursor_new(GDK_WATCH);
		}
		if(((GTK_WIDGET(win->data))->window != NULL)&&(cursor != NULL)){
			gdk_window_set_cursor((GTK_WIDGET(win->data))->window,
					      cursor);
		}
		gdk_window_set_cursor((GTK_WIDGET(win->data))->window, cursor);
		win = g_list_next(win);
	}
}

int window_list_length(void)
{
	return g_list_length(window_list);
}
