/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

static void grab_focus(GtkWidget *widget)
{
	if(!GTK_IS_WIDGET(widget)) {
		return;
	}
	if(widget == gtk_widget_get_toplevel(widget)) {
		return;
	}

	if(GTK_IS_WIDGET(widget->parent)) {
		grab_focus(widget->parent);
	}

	if(GTK_IS_NOTEBOOK(widget->parent)) {
		int n = gtk_notebook_page_num(GTK_NOTEBOOK(widget->parent),
					      widget);
		gtk_notebook_set_page(GTK_NOTEBOOK(widget->parent), n);
	} else {
		gtk_widget_grab_focus(widget);
	}
}

HANDLER(curfield)
{
	GtkWidget *widget = NULL;
	char *id = (char*)g_list_nth_data(args, 0);

	g_return_val_if_fail(id != NULL, NULL);

	widget = get_widget_by_name(id);

	if(GTK_IS_WIDGET(widget)) {
		grab_focus(widget);
	} else {
		g_warning("Curfield: widget \"%s\" not found.\n", id);
	}

	return NULL;
}
