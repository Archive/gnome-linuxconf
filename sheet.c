/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

static void sheet_add_item(GtkWidget *button, GtkWidget *clist)
{
}

DUMPER(sheet)
{
	int columns, rows, i, j;
	const char *cname = get_widget_relative_name(dumpee);
	char *path, *c, *text;

	g_return_if_fail(cname != NULL);
	columns = (GTK_CLIST(dumpee))->columns;
	rows = (GTK_CLIST(dumpee))->rows;

	path = g_strdup(cname);
	c = strrchr(path, '.');
	if(c != NULL) {
		*c++ = '\0';
#ifdef DEBUG
		printf("dump %s %s %d\n", path, c, columns);
#endif
		g_print("dump %s %s %d\n", path, c, columns);
		for(i = 0; i < columns; i++)
		for(j = 0; j < rows; j++) {
			gtk_clist_get_text(GTK_CLIST(dumpee), i, j, &text);
#ifdef DEBUG
			printf("dump %s %s-%d-%d %s\n", path, c, i, j, text);
#endif
			g_print("dump %s %s-%d-%d %s\n", path, c, i, j, text);
		}
	}
	g_free(path);
}

HANDLER(sheet)
{
	GtkWidget *widget, *vbox, *button, *sw, *pm;
	char *id = (char*)g_list_nth_data(args, 0);
	char *nbcol = (char*)g_list_nth_data(args, 1);
	int i;

	g_return_val_if_fail(id != NULL, NULL);
	g_return_val_if_fail(nbcol != NULL, NULL);
	g_return_val_if_fail(atoi(nbcol) > 0, NULL);

	widget = gtk_clist_new(atoi(nbcol));
	gtk_clist_set_selection_mode(GTK_CLIST(widget), GTK_SELECTION_BROWSE);
	for(i = 0; i < atoi(nbcol); i++) {
		char *tmp = g_strdup(g_list_nth_data(args, 2 + i));
		gtk_clist_set_column_title(GTK_CLIST(widget), i, tmp);
		gtk_signal_connect(GTK_OBJECT(widget), "destroy",
				   GTK_SIGNAL_FUNC(free_data), tmp);
	}
	sw = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw),
				       GTK_POLICY_AUTOMATIC,
				       GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(sw), widget);

	vbox = gtk_vbox_new(FALSE, 0);

	pm = gnome_stock_pixmap_widget_new(vbox, GNOME_STOCK_PIXMAP_ADD);
	button = gnome_pixmap_button(pm, GNOME_STOCK_PIXMAP_ADD);
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
			   GTK_SIGNAL_FUNC(sheet_add_item), widget);

	gtk_box_pack_start(GTK_BOX(vbox), sw, TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 0);

	parent_insert(NULL, vbox);

	return widget;
}

HANDLER(sheetitem)
{
	GtkWidget *widget;
	char *id = (char*)g_list_nth_data(args, 0);
	char *row = (char*)g_list_nth_data(args, 1);
	char *column = (char*)g_list_nth_data(args, 2);
	char *value = concat_strings(g_list_next(g_list_next(g_list_next(args))));

	g_return_val_if_fail(id != NULL, NULL);
	g_return_val_if_fail(row != NULL, NULL);
	g_return_val_if_fail(atoi(row) != 0, NULL);
	g_return_val_if_fail(column != NULL, NULL);
	g_return_val_if_fail(atoi(column) != 0, NULL);

	widget = get_widget_by_name(id);
	g_return_val_if_fail(GTK_IS_CLIST(widget), NULL);

	gtk_clist_set_text(GTK_CLIST(widget), atoi(row), atoi(column), value);
	gtk_signal_connect(GTK_OBJECT(widget), "destroy",
			   GTK_SIGNAL_FUNC(free_data), value);

	return NULL;
}
