Summary: The GNOME front-end for linuxconf.
Name: gnome-linuxconf
Version: 0.33
Release: 7
Copyright: GPL
Group: Applications/System
Source: gnome-linuxconf-%{version}.tar.gz
BuildRoot: %{_tmppath}/gnome-linuxconf-root
URL: http://www.gnome.org/
Requires: linuxconf >= 1.17
Conflicts: linuxconf < 1.17
BuildPrereq: linuxconf-devel
# requires newer protocol stuffs

%description
GNOME (GNU Network Object Model Environment) is a user-friendly set of
GUI applications and desktop tools to be used in conjunction with a window
manager for the X Window System. The gnome-linuxconf package includes
GNOME's front-end for the linuxconf system configuration utility.

You should install the gnome-linuxconf package if you would like to use
GNOME's linuxconf interface.


%prep
%setup -q

%build
%configure
make

%install
[ "$RPM_BUILD_ROOT" != "/" ] && rm -fr $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
strip $RPM_BUILD_ROOT%{_prefix}/bin/*
mkdir -p $RPM_BUILD_ROOT%{_prefix}/lib/rhs/control-panel/
install -m 644 linuxconf.xpm $RPM_BUILD_ROOT%{_prefix}/lib/rhs/control-panel/
install -m 755 linuxconf.init $RPM_BUILD_ROOT%{_prefix}/lib/rhs/control-panel/
mkdir -p $RPM_BUILD_ROOT/etc/X11/applnk/System
install -m 600 conf.desktop $RPM_BUILD_ROOT/etc/X11/applnk/System/conf.desktop

%clean
[ "$RPM_BUILD_ROOT" != "/" ] && rm -fr $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{_prefix}/bin/gnome-linuxconf
%{_prefix}/lib/rhs/control-panel/*
%config /etc/X11/applnk/System/conf.desktop

%changelog
* Mon Aug 21 2000 Nalin Dahyabhai <nalin@redhat.com>
- remove filler space between buttonfill components

* Fri Jul 21 2000 Nalin Dahyabhai <nalin@redhat.com>
- fully-qualify path to linuxconf we provide to control-panel

* Tue Jul 18 2000 Nalin Dahyabhai <nalin@redhat.com>
- allow apostrophes in strings

* Thu Jul 13 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Wed Jul  5 2000 Nalin Dahyabhai <nalin@redhat.com>
- don't set window minimum width

* Mon Jul 03 2000 Trond Eivind Glomsr�d <teg@redhat.com>
- add default owner

* Sun Jul  2 2000 Nalin Dahyabhai <nalin@redhat.com>
- stub out the new drawing commands to avoid printing useless error messages

* Fri May 19 2000 Nalin Dahyabhai <nalin@redhat.com>
- change list items with comments to use tooltips
- sort out initial window sizing, I think
- make boxes scrollable
- fix clist insertion problems

* Fri May 12 2000 Nalin Dahyabhai <nalin@redhat.com>
- add reconfdia support
- fix gauges
- add setval support for labels, text fields, and gauges

* Thu May  4 2000 Nalin Dahyabhai <nalin@redhat.com>
- repackage with new engine

* Tue Feb 08 2000 Nalin Dahyabhai <nalin@redhat.com>
- strip the binaries, tag files under /etc as config files

* Thu Feb 03 2000 Nalin Dahyabhai <nalin@redhat.com>
- some tweaks to hopefully make it look better, other fixes

* Sun Sep 19 1999 Michael K. Johnson <johnsonm@redhat.com>
- work around some gtk misfeatures

* Tue Apr 06 1999 Michael K. Johnson <johnsonm@redhat.com>
- button padding
- a little bigger by default
- notebook tab scrolling turned on
- replaced wmconfig file with applnk file

* Thu Apr 01 1999 Michael K. Johnson <johnsonm@redhat.com>
- exit cleanly when linuxconf sends clist items without a clist

* Thu Mar 25 1999 Michael K. Johnson <johnsonm@redhat.com>
- fix bug in choice menu state reporting for default choice

* Mon Mar 15 1999 Michael K. Johnson <johnsonm@redhat.com>
- honor and report the menu tree state
- new summary/description/group

* Sun Feb 07 1999 Michael K. Johnson <johnsonm@redhat.com>
- New version now builds against Gtk 1.1.15 as well

* Mon Jan 25 1999 Michael K. Johnson <johnsonm@redhat.com>
- new improved version, should build both with GNOME 0.20/Gtk 1.0.x
  and with GNOME 0.99.x/Gtk 1.1.x

* Thu Oct 29 1998 Michael K. Johnson <johnsonm@redhat.com>
- imported back into official gnome tree, new version to celebrate!

* Thu Oct 01 1998 Michael K. Johnson <johnsonm@redhat.com>
- library fix

* Mon Sep 28 1998 Michael K. Johnson <johnsonm@redhat.com>
- fixed lots of bugs, now has treemenu mode

* Thu May 07 1998 Michael K. Johnson <johnsonm@redhat.com>
- linuxconf 1.11r11 handles fast connections appropriately.

* Tue May 05 1998 Michael K. Johnson <johnsonm@redhat.com>
- ignore SIGTSTP to work around suspected gtk bug
- fix use of vsnprintf()
- fix port 98 remadmin connections
- fixed resizing bug without completely changing layout

* Mon May 04 1998 Michael K. Johnson <johnsonm@redhat.com>
- fixed resizing bug
- added wmconfig entry for linuxconf

* Sat May 02 1998 Michael K. Johnson <johnsonm@redhat.com>
- fixed radio buttons, deal with "rich text"
- improved justification follows remadmin standard more closely

* Fri May 01 1998 Michael K. Johnson <johnsonm@redhat.com>
- fixes radio buttons and combo boxes
- work around small gnome problems

* Wed Apr 29 1998 Michael K. Johnson <johnsonm@redhat.com>
- takes title widths into account, so works with 0-length clists

* Thu Apr 16 1998 Michael K. Johnson <johnsonm@redhat.com>
- Now works with linuxconf 1.10r21
- Fixed a few more display problems.

* Wed Apr 15 1998 Michael K. Johnson <johnsonm@redhat.com>
- New io engine, some other improvements, make this really usable.

* Thu Apr 09 1998 Michael K. Johnson <johnsonm@redhat.com>
- Make a linuxconf-only package.  Final intent is to split gnome
  packages out more finely-grained than the current CVS tree.

* Mon Mar 16 1998 Marc Ewing <marc@redhat.com>
- Integrate into gnome-admin CVS source tree
