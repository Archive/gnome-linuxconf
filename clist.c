/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

static void clistitem_select(GtkCList *clist, gint row, gint column,
	       		     GdkEventButton *event, gpointer data)
{
	char *text = NULL;

	g_return_if_fail(mainform_of(GTK_WIDGET(clist)) != NULL);
	text = gtk_clist_get_row_data(clist, row);

	if(text != NULL) {
#ifdef DEBUG
		printf("action %s %s 1\n",
		       get_widget_relative_name(GTK_WIDGET(clist)), text);
#endif
		g_print("action %s %s 1\n",
			get_widget_relative_name(GTK_WIDGET(clist)), text);
	}
}

DUMPER(clist)
{
}

HANDLER(clist)
{
	GtkWidget *widget = NULL, *sw = NULL;
	GtkScrolledWindow *s = NULL;
	char *id = (char*)g_list_nth_data(args, 0);
	char *nbcol = (char*)g_list_nth_data(args, 1);
	int i;

	g_return_val_if_fail(id != NULL, NULL);
	g_return_val_if_fail(atoi(nbcol) != 0, NULL);

	widget = gtk_clist_new(atoi(nbcol));
	gtk_clist_set_selection_mode(GTK_CLIST(widget), GTK_SELECTION_BROWSE);
	gtk_clist_column_titles_show(GTK_CLIST(widget));
	for(i = 0; i < atoi(nbcol); i++) {
		char *title = (char*)g_list_nth_data(args, 2 + i);
		title = title ? g_strdup(title) : g_strdup("");
		gtk_clist_set_column_title(GTK_CLIST(widget), i, title);
		gtk_signal_connect(GTK_OBJECT(widget), "destroy",
				   GTK_SIGNAL_FUNC(free_data), title);
		gtk_clist_set_column_min_width(GTK_CLIST(widget),
			       		       i, i ? 50 : 100);
	}
	gtk_signal_connect(GTK_OBJECT(widget), "select-row",
			   GTK_SIGNAL_FUNC(clistitem_select), NULL);

	sw = gtk_scrolled_window_new(NULL, NULL);
	s = GTK_SCROLLED_WINDOW(sw);

	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw),
				       GTK_POLICY_AUTOMATIC,
				       GTK_POLICY_AUTOMATIC);
	gtk_clist_set_hadjustment(GTK_CLIST(widget),
				  gtk_scrolled_window_get_hadjustment(s));
	gtk_clist_set_vadjustment(GTK_CLIST(widget),
				  gtk_scrolled_window_get_vadjustment(s));
	gtk_container_add(GTK_CONTAINER(sw), widget);

	parent_insert(NULL, sw);

	return widget;
}

HANDLER(clistitem)
{
	GtkWidget *clist;
	char *id = (char*)g_list_nth_data(args, 0);
	char *item_id = (char*)g_list_nth_data(args, 1);
	char **nul = NULL;
	int i, row;

	g_return_val_if_fail(id != NULL, NULL);
	g_return_val_if_fail(item_id != NULL, NULL);

	clist = get_widget_by_name(id);
	g_return_val_if_fail(GTK_IS_CLIST(clist), NULL);
	nul = g_malloc0(sizeof(char*) * ((GTK_CLIST(clist))->columns + 1));
	row = gtk_clist_append(GTK_CLIST(clist), nul);
	g_free(nul);

	for(i = 0; i < (GTK_CLIST(clist))->columns; i++) {
		char *str = (char*)g_list_nth_data(args, 2 + i);

		str = str ? g_strdup(str) : g_strdup("");
		gtk_clist_set_text(GTK_CLIST(clist), row, i, str);
		gtk_signal_connect(GTK_OBJECT(clist), "destroy",
				   GTK_SIGNAL_FUNC(free_data), str);
	}

	item_id = g_strdup(item_id);
	gtk_clist_set_row_data(GTK_CLIST(clist), row, item_id);
	gtk_signal_connect(GTK_OBJECT(clist), "destroy",
			   GTK_SIGNAL_FUNC(free_data), item_id);

	return NULL;
}
