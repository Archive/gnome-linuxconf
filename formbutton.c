/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

DUMPER(formbutton)
{
}

HANDLER(formbutton)
{
	GtkWidget *widget = NULL;

	gtk_hbutton_box_set_layout_default(GTK_BUTTONBOX_SPREAD);
	widget = gtk_hbutton_box_new();

        gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_VFLAGS,
                            GINT_TO_POINTER(GTK_SHRINK));
        gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_VPAD,
                            GINT_TO_POINTER(-1));

	parent_insert(NULL, widget);
	parents_push(widget);

	return widget;
}
