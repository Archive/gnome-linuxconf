/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

HANDLER(skip)
{
	int i;
	for(i = 0; i < atoi((char*)g_list_nth_data(args, 0)); i++) {
		parent_insert(NULL, NULL);
	}
	return NULL;
}
