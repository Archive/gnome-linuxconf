/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

DUMPER(vline)
{
}

HANDLER(vline)
{
	GtkWidget *widget = NULL, *label = NULL;
	char *id = (char*)g_list_nth_data(args, 0);
	char *text = concat_strings(g_list_next(args));

	g_return_val_if_fail(id != NULL, NULL);

	widget = gtk_vbox_new(FALSE, 0);

	gtk_box_pack_start(GTK_BOX(widget), gtk_vseparator_new(),
			   FALSE, FALSE, 0);

	if(strlen(text) > 0) {
		label = gtk_label_new(text);
		gtk_signal_connect(GTK_OBJECT(label), "destroy",
				   GTK_SIGNAL_FUNC(free_data), text);
		gtk_box_pack_start(GTK_BOX(widget), label,
			       	   TRUE, TRUE, GNOME_PAD_SMALL);

		gtk_box_pack_start(GTK_BOX(widget), gtk_vseparator_new(),
				   FALSE, FALSE, 0);
	} else {
		g_free(text);
	}

	parent_insert(NULL, widget);

	return NULL;
}
