/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

DUMPER(choice)
{
	/* Dump: "dump form-path base-id value\n" */
	GtkWidget *active, *menu;
	const char *cname = get_widget_relative_name(dumpee);
	char *path, *c, *value;

	g_return_if_fail(GTK_IS_OPTION_MENU(dumpee));
	g_return_if_fail(cname != NULL);

	menu = gtk_option_menu_get_menu(GTK_OPTION_MENU(dumpee));
	g_return_if_fail(GTK_IS_MENU(menu));

	active = gtk_menu_get_active(GTK_MENU(menu));
	g_return_if_fail(GTK_IS_MENU_ITEM(active));

	value = gtk_object_get_data(GTK_OBJECT(active),
		       		    GNOME_LINUXCONF_CHOICE_VALUE);
	g_return_if_fail(value != NULL);

	path = g_strdup(cname);
	c = strrchr(path, '.');
	if(c != NULL) {
		*c++ = '\0';
#ifdef DEBUG
		printf("dump %s %s %s\n", path, c, value);
#endif
		g_print("dump %s %s %s\n", path, c, value);
	}
}

HANDLER(choice)
{
	GtkWidget *option_menu, *widget = NULL;
	GtkTooltips *tooltips = NULL;
	char *id = (char*)g_list_nth_data(args, 0);
	char *value = concat_strings(g_list_next(args));

	g_return_val_if_fail(id != NULL, NULL);
	g_return_val_if_fail(value != NULL, NULL);

	option_menu = gtk_option_menu_new();
	gtk_object_set_data(GTK_OBJECT(option_menu),
			    GNOME_LINUXCONF_CHOICE_DEFAULT, value);
	gtk_object_set_data(GTK_OBJECT(option_menu),
			    GNOME_LINUXCONF_HINT_VFLAGS,
			    GINT_TO_POINTER(GTK_SHRINK));
	gtk_signal_connect(GTK_OBJECT(option_menu), "destroy",
			   GTK_SIGNAL_FUNC(free_data), value);
	widget = gtk_menu_new();
	gtk_option_menu_set_menu(GTK_OPTION_MENU(option_menu), widget);

	tooltips = gtk_tooltips_new();
	gtk_tooltips_enable(tooltips);
	gtk_object_set_data(GTK_OBJECT(option_menu),
			    GNOME_LINUXCONF_CHOICE_TIPS, tooltips);
	gtk_signal_connect_object(GTK_OBJECT(option_menu), "destroy",
				  GTK_SIGNAL_FUNC(gtk_object_destroy),
				  GTK_OBJECT(tooltips));

	parent_insert(NULL, option_menu);

	return option_menu;
}

HANDLER(choiceitem)
{
	GtkWidget *widget = NULL, *option_menu, *menu, *label;
	GtkTooltips *tooltips;
	char *id = (char*)g_list_nth_data(args, 0);
	char *value = (char*)g_list_nth_data(args, 1);
	char *comments = concat_strings(g_list_next(g_list_next(args)));
	char *defval;

	g_return_val_if_fail(id != NULL, NULL);
	g_return_val_if_fail(value != NULL, NULL);

	option_menu = get_widget_by_name(id);
	g_return_val_if_fail(GTK_IS_OPTION_MENU(option_menu), NULL);

	menu = gtk_option_menu_get_menu(GTK_OPTION_MENU(option_menu));

	tooltips = gtk_object_get_data(GTK_OBJECT(option_menu),
				       GNOME_LINUXCONF_CHOICE_TIPS);

	value = g_strdup(value);
	label = gtk_label_new(value);
	gtk_signal_connect(GTK_OBJECT(label), "destroy",
			   GTK_SIGNAL_FUNC(free_data), value);

	widget = gtk_menu_item_new();
	gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_CHOICE_VALUE,
			    value);
	gtk_container_add(GTK_CONTAINER(widget), label);

	if((tooltips != NULL) && (strlen(comments) > 0)) {
		gtk_tooltips_set_tip(tooltips, widget, comments, NULL);
	}

	gtk_menu_append(GTK_MENU(menu), widget);

	defval = gtk_object_get_data(GTK_OBJECT(option_menu),
				     GNOME_LINUXCONF_CHOICE_DEFAULT);

	if((defval != NULL) && (strcmp(defval, value) == 0)) {
		int n;
		n = g_list_length((GTK_MENU_SHELL(menu))->children);
		gtk_option_menu_set_history(GTK_OPTION_MENU(option_menu), n - 1);
	}

	return NULL;
}
