/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

static void action_button(GtkWidget *widget, gpointer data)
{
	GtkWidget *mainform;
	const char *name;
	const char *mainform_name;
	char *parent_name = NULL;
	int keyboard_state = 0;

	g_assert(GTK_IS_BUTTON(widget));
	name = get_widget_relative_name(widget);
	g_return_if_fail(name != NULL);

	/* Disable all windows while Linuxconf processes. */
	window_list_busy_all(NULL, NULL);

	/* Get the name of the mainform we're in. */
	mainform = mainform_of(widget);
	g_return_if_fail(GTK_IS_WIDGET(mainform));
	mainform_name = get_widget_base_name(mainform);

	/* Dump widgets of the subform. */
	if(gtk_object_get_data(GTK_OBJECT(widget), GNOME_LINUXCONF_NEEDS_DUMP)){
		gtk_container_foreach(GTK_CONTAINER(mainform),
				      dump_widget, NULL);
	}

	/* Get the parent widget's name. */
	parent_name = g_strdup(name);
	if(strrchr(parent_name, '.') != NULL) {
		*(strrchr(parent_name, '.')) = '\0';
	}
	/* Tell the back-end we were clicked. */
#ifdef DEBUG
	printf("action %s %s %d\n", strstr(parent_name, mainform_name),
	       get_widget_base_name(widget), keyboard_state);
#endif
	g_print("action %s %s %d\n", strstr(parent_name, mainform_name),
		get_widget_base_name(widget), keyboard_state);

	g_free(parent_name);
}

/* A regular button. */
DUMPER(button)
{
}

HANDLER(button)
{
	GtkWidget *widget = NULL;
	char *id = (char*)g_list_nth_data(args, 0);
	char *dump = g_list_nth_data(args, 1);
	char *title = (char*)g_list_nth_data(args, 2);

	/* If title is not there, it's because dump was omitted. */
	if(title == NULL) {
		title = dump;
		dump = NULL;
	}

	g_return_val_if_fail(id != NULL, NULL);
	g_return_val_if_fail(title != NULL, NULL);

	/* Create a new label for the widget text. */
	title = g_strdup(title);
	widget = gtk_button_new_with_label(title);
	gtk_signal_connect(GTK_OBJECT(widget), "clicked",
			   GTK_SIGNAL_FUNC(action_button), NULL);
	gtk_signal_connect(GTK_OBJECT(widget), "destroy",
			   GTK_SIGNAL_FUNC(free_data), title);
	gtk_misc_set_padding(GTK_MISC((GTK_BIN(widget))->child), GNOME_PAD, 0);

	if((dump != NULL) && (atoi(dump) != 0)) {
		gtk_object_set_data(GTK_OBJECT(widget),
				    GNOME_LINUXCONF_NEEDS_DUMP,
				    GNOME_LINUXCONF_NEEDS_DUMP);
	}

        gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_HFLAGS,
                            GINT_TO_POINTER(GTK_EXPAND | GTK_FILL));
        gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_VFLAGS,
                            GINT_TO_POINTER(GTK_SHRINK));
        gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_VPAD,
                            GINT_TO_POINTER(GNOME_PAD_SMALL));

	parent_insert(NULL, widget);

	return widget;
}

/* A button with a graphic in it. */
DUMPER(buttonxpm)
{
}

HANDLER(buttonxpm)
{
	GtkWidget *widget = NULL, *pm;
	char *id = (char*)g_list_nth_data(args, 0);
	char *icon_name = g_list_nth_data(args, 1);
	char *title = concat_strings(g_list_next(g_list_next(args)));

	g_return_val_if_fail(id != NULL, NULL);
	g_return_val_if_fail(icon_name != NULL, NULL);
	g_return_val_if_fail(title != NULL, NULL);

	if(strcmp(title, "-") != 0) {
		widget = gtk_button_new_with_label(title);
		gtk_signal_connect(GTK_OBJECT(widget), "clicked",
				   GTK_SIGNAL_FUNC(action_button), NULL);
	} else {
		pm = get_widget_by_name(icon_name);
		widget = gnome_pixmap_new_from_gnome_pixmap(GNOME_PIXMAP(pm));
	}

        gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_HFLAGS,
                            GINT_TO_POINTER(GTK_EXPAND | GTK_FILL));
        gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_VFLAGS,
                            GINT_TO_POINTER(GTK_SHRINK));
        gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_VPAD,
                            GINT_TO_POINTER(GNOME_PAD_SMALL));

	parent_insert(NULL, widget);

	return GTK_IS_BUTTON(widget) ? widget : NULL;
}

/* A button with a graphic (given by a filename) in it. */
DUMPER(buttonxpmf)
{
}

HANDLER(buttonxpmf)
{
	GtkWidget *widget = NULL, *pm;

	char *id = (char*)g_list_nth_data(args, 0);
	char *icon_file = g_list_nth_data(args, 1);
	char *title = concat_strings(g_list_next(g_list_next(args)));

	g_return_val_if_fail(id != NULL, NULL);
	g_return_val_if_fail(icon_file != NULL, NULL);

	/* Try to load the pixmap. */
	pm = gnome_pixmap_new_from_file(icon_file);
	if(GNOME_IS_PIXMAP(pm)) {
		pm = gnome_pixmap_new_from_gnome_pixmap(GNOME_PIXMAP(pm));
	}

	/* If it's just for the icon, make it a pixmap instead. */
	if(strcmp(title, "-") == 0) {
		widget = pm;
		g_free(title);
	} else {
		widget = gnome_pixmap_button(pm, title);
		gtk_signal_connect(GTK_OBJECT(widget), "clicked",
				   GTK_SIGNAL_FUNC(action_button), NULL);
		gtk_signal_connect(GTK_OBJECT(widget), "destroy",
				   GTK_SIGNAL_FUNC(free_data), title);
	}

        gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_HFLAGS,
                            GINT_TO_POINTER(GTK_EXPAND | GTK_FILL));
        gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_VFLAGS,
                            GINT_TO_POINTER(GTK_SHRINK));
        gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_VPAD,
                            GINT_TO_POINTER(GNOME_PAD_SMALL));

	parent_insert(NULL, widget);

	return GTK_IS_BUTTON(widget) ? widget : NULL;
}

DUMPER(buttonfill)
{
}

HANDLER(buttonfill)
{
	GtkWidget *widget = NULL;
	char *id = (char*)g_list_nth_data(args, 0);
	char *dump = g_list_nth_data(args, 1);
	char *title = (char*)g_list_nth_data(args, 2);

	/* If title is not there, it's because dump was omitted. */
	if(title == NULL) {
		title = dump;
		dump = NULL;
	}

	g_return_val_if_fail(id != NULL, NULL);
	g_return_val_if_fail(title != NULL, NULL);

	/* Create a new label for the widget text. */
	title = g_strdup(title);
	widget = gtk_button_new_with_label(title);
	gtk_signal_connect(GTK_OBJECT(widget), "clicked",
			   GTK_SIGNAL_FUNC(action_button), NULL);
	gtk_signal_connect(GTK_OBJECT(widget), "destroy",
			   GTK_SIGNAL_FUNC(free_data), title);
	gtk_misc_set_padding(GTK_MISC((GTK_BIN(widget))->child), GNOME_PAD, 0);

	if((dump != NULL) && (atoi(dump) != 0)) {
		gtk_object_set_data(GTK_OBJECT(widget),
				    GNOME_LINUXCONF_NEEDS_DUMP,
				    GNOME_LINUXCONF_NEEDS_DUMP);
	}

	gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_VFLAGS,
			    GINT_TO_POINTER(GTK_SHRINK));
	gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_VPAD,
			    GINT_TO_POINTER(GNOME_PAD_SMALL));
	gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_HPAD,
			    GINT_TO_POINTER(-1));
	parent_insert(NULL, widget);

	return widget;
}
