/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

DUMPER(checkbox)
{
	const char *cname = get_widget_relative_name(dumpee);
	char *name, *c;
	gboolean cb;

	cb = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(dumpee));

	g_return_if_fail(cname != NULL);
	name = g_strdup(cname);
	c = strrchr(name, '.');

	if(c != NULL) {
		*c++ = '\0';
#ifdef DEBUG
		printf ("dump %s %s %d\n", name, c, cb ? 1 : 0);
#endif
		g_print("dump %s %s %d\n", name, c, cb ? 1 : 0);
	}

	g_free(name);
}

HANDLER(checkbox)
{
	GtkWidget *widget = NULL;
	char *id = (char*)g_list_nth_data(args, 0);
	char *value = g_list_nth_data(args, 1);
	char *title = (char*)g_list_nth_data(args, 2);

	g_return_val_if_fail(id != NULL, NULL);
	g_return_val_if_fail(value != NULL, NULL);
	g_return_val_if_fail(title != NULL, NULL);

	widget = gtk_check_button_new_with_label(title);
	if((value != NULL) && (atoi(value) != 0)) {
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(widget), TRUE);
	}

	gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_VFLAGS,
			    GINT_TO_POINTER(GTK_SHRINK));
	gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_VPAD,
			    GINT_TO_POINTER(-1));

	parent_insert(NULL, widget);
	return widget;
}
