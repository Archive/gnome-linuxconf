/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"
#define NORMAL_FONT "-*-helvetica-medium-r-*-*-12-*-*-*-*-*-*-*"
#define BOLD_FONT "-*-helvetica-bold-r-*-*-12-*-*-*-*-*-*-*"
#define OBLIQUE_FONT "-*-lucida-medium-i-*-*-12-*-*-*-*-*-*-*"

DUMPER(richtext)
{
}

HANDLER(richtext)
{
	GtkWidget *widget, *child;
	char *text = concat_strings(args);
	static GdkFont *boldfont = NULL;
	static GdkFont *obliquefont = NULL;
	gboolean bold = FALSE, underline = FALSE, rich = FALSE;
	guint i;

	g_return_val_if_fail(text != NULL, NULL);

	widget = gtk_hbox_new(FALSE, 0);

	for(i = 0, rich = FALSE; text[i] != '\0'; i++) {
		if((text[i] == '') || (text[i] == '_')) {
			rich = TRUE;
		}
	}
	if(rich) {
		for(i = 0; text[i] != '\0'; i++) {
			char c = text[i];
			underline = bold = FALSE;
			if(text[i + 1] == '') {
				if(text[i] == '_') {
					underline = TRUE;
					c = text[i + 2];
				} else if(text[i + 2] == '_') {
					underline = TRUE;
					c = text[i];
				} else {
					bold = TRUE;
					c = text[i + 2];
				}
				i += 2;
			}
			if(text[i] != '') {
				GtkStyle *style;
				char *child_text = g_strndup(&c, 1);
				child = gtk_label_new(child_text);
				gtk_signal_connect(GTK_OBJECT(child), "destroy",
						   GTK_SIGNAL_FUNC(free_data),
						   child_text);
				style = gtk_widget_get_style(child);
				if(bold) {
					if(boldfont == NULL) {
						boldfont = gdk_font_load(BOLD_FONT);
					}
					if(boldfont != NULL) {
						style = gtk_style_copy(style);
						style->font = boldfont;
						gtk_widget_set_style(child, style);
					}
				}
				if(underline) {
					if(obliquefont == NULL) {
						obliquefont = gdk_font_load(OBLIQUE_FONT);
					}
					if(obliquefont != NULL) {
						style = gtk_style_copy(style);
						style->font = obliquefont;
						gtk_widget_set_style(child, style);
					}
				}
				gtk_box_pack_start(GTK_BOX(widget), child,
						   FALSE, FALSE, 0);
			}
		}
	} else {
		char *child_text = g_strdup(text);
		child = gtk_label_new(child_text);
		gtk_signal_connect(GTK_OBJECT(child), "destroy",
				   GTK_SIGNAL_FUNC(free_data),
				   child_text);
		gtk_box_pack_start(GTK_BOX(widget), child, FALSE, FALSE, 0);
	}
	g_free(text);

	gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_VFLAGS,
			    GINT_TO_POINTER(GTK_SHRINK));
	gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_VPAD,
			    GINT_TO_POINTER(-1));

	parent_insert(NULL, widget);

	return NULL;
}
