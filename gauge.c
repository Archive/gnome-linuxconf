/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"
#define GAUGE_HEIGHT GNOME_PAD

DUMPER(gauge)
{
}

HANDLER(gauge)
{
	GtkWidget *widget = NULL;
	char *id = (char*)g_list_nth_data(args, 0);
	char *width = (char*)g_list_nth_data(args, 1);
	char *range = (char*)g_list_nth_data(args, 2);
	char *value = (char*)g_list_nth_data(args, 3);
	float fwidth = 0;

	g_return_val_if_fail(id != NULL, NULL);
	g_return_val_if_fail(width != NULL, NULL);
	g_return_val_if_fail(range != NULL, NULL);
	g_return_val_if_fail(value != NULL, NULL);

	widget = gtk_progress_bar_new();
	gtk_progress_configure(GTK_PROGRESS(widget), atof(value),
		       	       0, atof(range));
	fwidth = atof(width);
	fwidth = CLAMP(fwidth * 1.5, 0, 600);
	gtk_widget_set_usize(GTK_WIDGET(widget), fwidth, GAUGE_HEIGHT);

	parent_insert(NULL, widget);

	return widget;
}
