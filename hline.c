/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

DUMPER(hline)
{
}

HANDLER(hline)
{
	GtkWidget *widget = NULL, *label = NULL;
	char *text = concat_strings(args);

	widget = gtk_hbox_new(FALSE, 0);

	gtk_box_pack_start(GTK_BOX(widget), gtk_hseparator_new(),
			   TRUE, TRUE, 0);

	if(strlen(text) > 0) {
		label = gtk_label_new(text);
		gtk_signal_connect(GTK_OBJECT(label), "destroy",
				   GTK_SIGNAL_FUNC(free_data), text);

		gtk_box_pack_start(GTK_BOX(widget), label,
			       	   FALSE, FALSE, GNOME_PAD_SMALL);

		gtk_box_pack_start(GTK_BOX(widget), gtk_hseparator_new(),
				   TRUE, TRUE, 0);
	} else {
		g_free(text);
	}

	parent_insert(NULL, widget);

	return NULL;
}
