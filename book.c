/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

GtkWidget *previous_notebook = NULL;

static void book_autoshow(GtkWidget *widget, gpointer data)
{
	if(g_list_length((GTK_NOTEBOOK(widget))->children) > 0) {
		gtk_widget_show(widget);
	} else {
		gtk_widget_hide(widget);
	}
}

DUMPER(book)
{
}

HANDLER(book)
{
	GtkWidget *notebook = NULL;
	char *id = (char*)g_list_nth_data(args, 0);

	g_return_val_if_fail(id != NULL, NULL);

	previous_notebook = notebook = gtk_notebook_new();
#if 1
	gtk_signal_connect(GTK_OBJECT(notebook), "map",
			   GTK_SIGNAL_FUNC(book_autoshow), NULL);
	gtk_signal_connect(GTK_OBJECT(notebook), "add",
			   GTK_SIGNAL_FUNC(book_autoshow), NULL);
	gtk_signal_connect(GTK_OBJECT(notebook), "remove",
			   GTK_SIGNAL_FUNC(book_autoshow), NULL);
#endif
	gtk_notebook_set_scrollable(GTK_NOTEBOOK(notebook), TRUE);

        gtk_object_set_data(GTK_OBJECT(notebook), GNOME_LINUXCONF_HINT_HPAD,
                            GINT_TO_POINTER(0));
        gtk_object_set_data(GTK_OBJECT(notebook), GNOME_LINUXCONF_HINT_VPAD,
                            GINT_TO_POINTER(0));

	parent_insert(NULL, notebook);
#if 0
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook),
				 gtk_label_new("Dummy page."),
				 gtk_label_new("Dummy page."));
#endif
	parents_push(notebook);

	return notebook;
}
