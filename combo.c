/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

DUMPER(combo)
{
	GtkWidget *entry;
	const char *text;
	const char *cname = get_widget_relative_name(dumpee);
       	char *name, *c;

	g_return_if_fail(cname != NULL);
	name = g_strdup(cname);
	c = strrchr(name, '.');

	if(c != NULL) {
		*c++ = '\0';
		entry = (GTK_COMBO(dumpee))->entry;
		text = gtk_entry_get_text(GTK_ENTRY(entry));
		if((text != NULL) && (strlen(text) != 0)) {
#ifdef DEBUG
			printf("dump %s %s %s\n", name, c, text);
#endif
			g_print("dump %s %s %s\n", name, c, text);
		}
	}

	g_free(name);
}

HANDLER(combo)
{
	GtkWidget *widget, *entry;
	GtkTooltips *tooltips;
	char *id = (char*)g_list_nth_data(args, 0);
	char *maxlen = (char*)g_list_nth_data(args, 1);
	char *val = concat_strings(g_list_next(g_list_next(args)));

	g_return_val_if_fail(id != NULL, NULL);
	g_return_val_if_fail(maxlen != NULL, NULL);

	widget = gtk_combo_new();
	gtk_combo_disable_activate(GTK_COMBO(widget));

	tooltips = gtk_tooltips_new();
	gtk_tooltips_enable(tooltips);
	gtk_signal_connect_object(GTK_OBJECT(widget), "destroy",
				  GTK_SIGNAL_FUNC(gtk_object_destroy),
				  GTK_OBJECT(tooltips));

	gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_COMBO_TIPS,
			    tooltips);
	entry = (GTK_COMBO(widget))->entry;
	gtk_entry_set_text(GTK_ENTRY(entry), val);
	gtk_object_set_data(GTK_OBJECT(entry), GNOME_LINUXCONF_COMBO_DEFAULT,
			    val);
	gtk_signal_connect(GTK_OBJECT(entry), "destroy",
			   GTK_SIGNAL_FUNC(free_data), val);
	if(atoi(maxlen)) {
		gtk_entry_set_max_length(GTK_ENTRY(entry), atoi(maxlen));
	}
	parent_insert(NULL, widget);

	return widget;
}

HANDLER(comboitem)
{
	GtkWidget *item, *combo, *label, *entry;
	GtkTooltips *tooltips;
	char *id = (char*)g_list_nth_data(args, 0);
	char *val = (char*)g_list_nth_data(args, 1);
	char *comments = concat_strings(g_list_next(g_list_next(args)));
	char *defval;

	combo = get_widget_by_name(id);
	g_return_val_if_fail(GTK_IS_COMBO(combo), NULL);
	g_return_val_if_fail(id != NULL, NULL);
	g_return_val_if_fail(val != NULL, NULL);

	tooltips = gtk_object_get_data(GTK_OBJECT(combo),
				       GNOME_LINUXCONF_COMBO_TIPS);

	entry = (GTK_COMBO(combo))->entry;
	defval = gtk_object_get_data(GTK_OBJECT(entry),
				     GNOME_LINUXCONF_COMBO_DEFAULT);

	item = gtk_list_item_new();

	val = g_strdup(val);
	label = gtk_label_new(val);
	gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_LEFT);
	gtk_signal_connect(GTK_OBJECT(label), "destroy",
			   GTK_SIGNAL_FUNC(free_data), val);
	gtk_container_add(GTK_CONTAINER(item), label);

	if((tooltips != NULL) && (strlen(comments) > 0)) {
		gtk_tooltips_set_tip(tooltips, item, comments, NULL);
	}

	val = g_strdup(val);
	gtk_combo_set_item_string(GTK_COMBO(combo), GTK_ITEM(item), val);
	gtk_signal_connect(GTK_OBJECT(item), "destroy",
			   GTK_SIGNAL_FUNC(free_data), val);
	gtk_container_add(GTK_CONTAINER((GTK_COMBO(combo))->list), item);

	gtk_widget_show_all(item);

	gtk_entry_set_text(GTK_ENTRY(entry), defval);

	return NULL;
}
