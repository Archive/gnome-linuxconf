/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

DUMPER(slider)
{
}

HANDLER(slider)
{
	GtkObject *adjustment = NULL;
	GtkWidget *widget = NULL;

	char *id = (char*)g_list_nth_data(args, 0);
	char *minval = (char*)g_list_nth_data(args, 0);
	char *maxval = (char*)g_list_nth_data(args, 0);

	g_return_val_if_fail(id != NULL, NULL);

	adjustment = gtk_adjustment_new(atof(minval),
		       			atof(minval),
					atof(maxval),
					1.0, 1.0, 1.0);
	widget = gtk_hscale_new(GTK_ADJUSTMENT(adjustment));
	parent_insert(NULL, widget);

	return widget;
}
