/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

GtkWidget *previous_mainform = NULL;

static gboolean action_window_delete(GtkWidget *widget, GdkEvent *event, gpointer data)
{
#ifdef DEBUG
	printf("action %s X\n", (char*) data);
#endif
	g_print("action %s X\n", (char*) data);
	return TRUE;
}

static void destroy_widget(GtkWidget *widget, gpointer data)
{
	gtk_widget_destroy(widget);
}

DUMPER(mainform)
{
}

HANDLER(mainform)
{
	GtkWidget *widget = NULL;

	char *id = (char*)g_list_nth_data(args, 0);
	char *title = (char*)g_list_nth_data(args, 1);
	char *type = (char*)g_list_nth_data(args, 2);
	g_return_val_if_fail(id != NULL, NULL);
	g_return_val_if_fail(title != NULL, NULL);
	type = (type != NULL) ? type : "std";

	/* Support the "reconfdia" primitive: if a dialog with the same name
	 * exists, then just remove its contents to preserve its position,
	 * size(?), etc. */
	if(GTK_IS_WIDGET(parents_peek())) {
		if(get_widget_name(GTK_WIDGET(parents_peek()))) {
			id = g_strdup_printf("%s.%s",
					     get_widget_name(parents_peek()),
					     id);
		}
	} else {
		id = g_strdup(id);
	}
	if(GTK_IS_WIDGET(get_widget_by_name(id))) {
		GtkWidget *dia = get_widget_by_name(id);

		if(GNOME_IS_APP(dia)) {
			GtkWidget *contents = (GNOME_APP(dia))->contents;
			gtk_widget_destroy(contents);
		} else {
			/* Remove the children. */
			if(GTK_IS_CONTAINER(dia)) {
				gtk_container_foreach(GTK_CONTAINER(dia),
						      destroy_widget, NULL);
			}
		}

		/* In case it held a table, clear the pointers. */
		gtk_object_set_data(GTK_OBJECT(dia),
				    GNOME_LINUXCONF_TABLE, NULL);
		gtk_object_set_data(GTK_OBJECT(dia),
				    GNOME_LINUXCONF_SW, NULL);

		g_free(id);

		parents_push(dia);

		return NULL;
	} else {
		/* Nope, it's a fresh mainform.  So return to what we were
		 * doing before... */
		g_free(id);
		id = (char*)g_list_nth_data(args, 0);
	}
	 
	/* This is screwed up.  Mainforms are supposed to be top-level windows,
	 * but LinuxConf seems to think otherwise, because it sometimes does a
	 * setContext right before opening up a new MainForm.  */
	if(parents_peek() != NULL) {
		/* Insert as a child of the current parent. NO TITLE FOR YOU! */
		widget = gtk_vbox_new(FALSE, 0);
		gtk_object_set_data(GTK_OBJECT(widget),
				    GNOME_LINUXCONF_HINT_HFLAGS,
				    GINT_TO_POINTER(GTK_EXPAND | GTK_FILL));
		gtk_object_set_data(GTK_OBJECT(widget),
				    GNOME_LINUXCONF_HINT_VFLAGS,
				    GINT_TO_POINTER(GTK_EXPAND | GTK_FILL));
		gtk_object_set_data(GTK_OBJECT(widget),
			            GNOME_LINUXCONF_HINT_HPAD,
				    GINT_TO_POINTER(-1));
		gtk_object_set_data(GTK_OBJECT(widget),
				    GNOME_LINUXCONF_HINT_VPAD,
				    GINT_TO_POINTER(-1));
	} else {
		/* It's a window.  Construct the right kind of window. */
		if(strcasecmp(type, "error") == 0) {
			widget = gtk_window_new(GTK_WINDOW_DIALOG);
		} else if(strcasecmp(type, "notice") == 0) {
			widget = gtk_window_new(GTK_WINDOW_DIALOG);
		} else if(strcasecmp(type, "popup") == 0) {
			widget = gtk_window_new(GTK_WINDOW_DIALOG);
		} else { /* "std" */
			widget = gnome_app_new(PACKAGE, title);
		}

		gtk_window_set_title(GTK_WINDOW(widget), title);
		gtk_window_set_policy(GTK_WINDOW(widget), FALSE, TRUE, TRUE);


		if((GTK_WINDOW(widget))->type != GTK_WINDOW_TOPLEVEL) {
#if 0
			gtk_window_set_position(GTK_WINDOW(widget),
						GTK_WIN_POS_MOUSE);
#else
			gtk_window_set_position(GTK_WINDOW(widget),
						GTK_WIN_POS_CENTER);
#endif
			window_list_unbusy_all(NULL, NULL);
		}
		window_list_add(widget, NULL);
		gtk_signal_connect(GTK_OBJECT(widget), "destroy",
				   GTK_SIGNAL_FUNC(window_list_remove),
				   NULL);
	}

	id = g_strdup(id);
	gtk_signal_connect(GTK_OBJECT(widget), "delete_event",
			   GTK_SIGNAL_FUNC(action_window_delete), id);
	gtk_signal_connect(GTK_OBJECT(widget), "destroy",
			   GTK_SIGNAL_FUNC(free_data), id);

	title = g_strdup(title);
	gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_TITLE, title);
	gtk_signal_connect(GTK_OBJECT(widget), "destroy",
			   GTK_SIGNAL_FUNC(free_data), title);

	if(parents_peek() != NULL) {
		parent_insert(NULL, widget);
	}

	previous_mainform = widget;
	parents_push(widget);

	return widget;
}
