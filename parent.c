/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

/* Functions to handle the "typewriter" metaphor used by LinuxConf in doing
 * parent-child layout stuff. */
void parent_insert(GtkWidget *parent, GtkWidget *child)
{
	GtkWidget *table = NULL, *sw = NULL, *viewport = NULL, *toplevel = NULL;
	int r, c, h, w;
	guint hflags, vflags;
	guint hpad, vpad;

	if(parent == NULL) {
		parent = parents_peek();
	}

	if(GTK_IS_NOTEBOOK(child) || GTK_IS_CTREE(child)) {
		if(GTK_IS_WINDOW(gtk_widget_get_toplevel(parent))) {
#ifdef DEBUG
			printf("Setting default window size.\n");
#endif
			gtk_window_set_default_size(GTK_WINDOW(gtk_widget_get_toplevel(parent)),
						    -1, GNOME_LINUXCONF_CLIP_SIZE);
		}
	}

	/* Adding a child to a notebook is apparently synonymous with adding
	 * a page.  Eeeeewww. */
	if(GTK_IS_NOTEBOOK(parent)) {
		char *title;
		GtkWidget *label;
		int pagenum;

	       	title = (char*) gtk_object_get_data(GTK_OBJECT(child),
			       			    GNOME_LINUXCONF_TITLE);
	       	label = title ? gtk_label_new(title) : NULL;

		gtk_notebook_append_page(GTK_NOTEBOOK(parent), child, label);

		pagenum = gtk_notebook_page_num(GTK_NOTEBOOK(parent), child);
		gtk_notebook_set_page(GTK_NOTEBOOK(parent), pagenum);

		return;
	}

	/* Get the address of the parent's table.  If it doesn't have one yet,
	 * create it here. */
	table = gtk_object_get_data(GTK_OBJECT(parent), GNOME_LINUXCONF_TABLE);
	if(table == NULL) {
		table = gtk_table_new(1, 1, FALSE);
		gtk_table_set_row_spacings(GTK_TABLE(table), 0);
		gtk_table_set_col_spacings(GTK_TABLE(table), 0);
		gtk_object_set_data(GTK_OBJECT(parent), GNOME_LINUXCONF_TABLE,
				    table);

		/* Based on what the parent is, insert the table differently. */
		if(GNOME_IS_APP(parent)) {
			/* Put the table into the app correctly. */
			gnome_app_set_contents(GNOME_APP(parent), table);
		} else
#if 0
		if(GTK_IS_BOX(parent)) {
			/* Pack the table into the box. */
			gtk_box_pack_start(GTK_BOX(parent), table, TRUE, TRUE,
					   GNOME_PAD_SMALL);
		} else
#endif
		{
			/* Pack the table into a viewport inside of a
			 * scrolled_windwo. */
			GtkAdjustment *hadj, *vadj;
			GtkScrolledWindow *s;

			sw = gtk_scrolled_window_new(NULL, NULL);
			s = (GtkScrolledWindow*) sw;

			hadj = gtk_scrolled_window_get_hadjustment(s);
			vadj = gtk_scrolled_window_get_vadjustment(s);

			viewport = gtk_viewport_new(hadj, vadj);

			gtk_scrolled_window_set_policy(s,
						       GTK_POLICY_NEVER,
						       GTK_POLICY_NEVER);

			gtk_container_add(GTK_CONTAINER(viewport), table);
			gtk_container_add(GTK_CONTAINER(sw), viewport);
			gtk_container_add(GTK_CONTAINER(parent), sw);

			gtk_object_set_data(GTK_OBJECT(parent),
					    GNOME_LINUXCONF_SW, sw);
		}
	}

	/* Get the address of the parent's scrolled window for later use. */
	if(sw == NULL) {
		gpointer scrolled = gtk_object_get_data(GTK_OBJECT(parent),
							GNOME_LINUXCONF_SW);
		if(GTK_IS_SCROLLED_WINDOW(scrolled)) {
			sw = GTK_WIDGET(GTK_SCROLLED_WINDOW(scrolled));
		}
	}

	/* Get the "current" row/column insertion position.  Note that if
	 * it's a new table, this is going to be zero. */
	r = GPOINTER_TO_INT(gtk_object_get_data(GTK_OBJECT(table),
						GNOME_LINUXCONF_CURRENT_ROW));
	c = GPOINTER_TO_INT(gtk_object_get_data(GTK_OBJECT(table),
						GNOME_LINUXCONF_CURRENT_COLUMN));

	/* Figure out how "wide" and "high" this widget should be when we
	 * insert it.  (Actually, the number of rows or columns to occupy.) */
	w = h = 0;
	if(GTK_IS_OBJECT(child)) {
		w = GPOINTER_TO_INT(gtk_object_get_data(GTK_OBJECT(child),
						GNOME_LINUXCONF_TABLE_WIDTH));
		h = GPOINTER_TO_INT(gtk_object_get_data(GTK_OBJECT(child),
						GNOME_LINUXCONF_TABLE_HEIGHT));
	}
	if(w == 0) {
		w = 1;
	}
	if(h == 0) {
		h = 1;
	}

	/* Unless we got a NULL child, that is... */
	if(GTK_IS_OBJECT(child)) {
		gpointer hint;

		/* Red widget-specific hints. */
		hint = gtk_object_get_data(GTK_OBJECT(child),
					   GNOME_LINUXCONF_HINT_HFLAGS);
		hflags = hint ? GPOINTER_TO_INT(hint) : GTK_EXPAND | GTK_FILL;

		hint = gtk_object_get_data(GTK_OBJECT(child),
					   GNOME_LINUXCONF_HINT_VFLAGS);
		vflags = hint ? GPOINTER_TO_INT(hint) : GTK_EXPAND | GTK_FILL;

		hint = gtk_object_get_data(GTK_OBJECT(child),
					   GNOME_LINUXCONF_HINT_HPAD);
		hpad =  hint ?
			CLAMP(GPOINTER_TO_INT(hint), 0, GNOME_PAD) :
			GNOME_PAD_SMALL;

		hint = gtk_object_get_data(GTK_OBJECT(child),
					   GNOME_LINUXCONF_HINT_VPAD);
		vpad =  hint ?
			CLAMP(GPOINTER_TO_INT(hint), 0, GNOME_PAD) :
			GNOME_PAD_SMALL;

#ifdef DEBUG
		printf("Attaching at %d-%d, %d-%d.\n", c, r, c + w, r + h);
#endif
		gtk_table_attach(GTK_TABLE(table), child, c, c + w, r, r + h,
				 hflags, vflags, hpad, vpad);
		gtk_widget_show(child);
	}

	/* Move the insertion point. */
	r = r + 0;
	c += w;

	gtk_object_set_data(GTK_OBJECT(table), GNOME_LINUXCONF_CURRENT_ROW,
			    GINT_TO_POINTER(r));
	gtk_object_set_data(GTK_OBJECT(table), GNOME_LINUXCONF_CURRENT_COLUMN,
			    GINT_TO_POINTER(c));

	/* If the window this widget is in has a ctree in it now, make sure it
	 * doesn't get too small. */
	if(GTK_IS_WIDGET(child)) {
		toplevel = gtk_widget_get_toplevel(child);
	}

	/* If the parent can scroll, we need to decide whether or not to let
	 * it. */
	if(GTK_IS_SCROLLED_WINDOW(sw)) {
		GtkWidget *viewport = NULL;

		viewport = (GTK_BIN(sw))->child;
#ifdef DEBUG
		printf("Parent is scrollable.\n");
#endif

		/* Switch on vertical scrolling if we think we might need it:
		 * if there are more than (magic number) rows and the child has
		 * no native scrolling. */
		if(!GTK_IS_SCROLLED_WINDOW(child) && 
		   !GTK_IS_NOTEBOOK(child) &&
		   ((GTK_TABLE(table))->nrows > 12)) {
#ifdef DEBUG
			printf("Setting parent to scrolling.\n");
#endif
			if(GTK_IS_VIEWPORT(viewport)) {
				gtk_viewport_set_shadow_type(GTK_VIEWPORT(viewport),
							     GTK_SHADOW_IN);
			}
			gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw),
						       GTK_POLICY_NEVER,
						       GTK_POLICY_AUTOMATIC);
			if(GTK_IS_WINDOW(toplevel)) {
				gtk_window_set_default_size(GTK_WINDOW(toplevel),
							    GNOME_LINUXCONF_CLIP_SIZE * 3 / 4,
							    GNOME_LINUXCONF_CLIP_SIZE * 3 / 4);
			}
		} else {
#ifdef DEBUG
			printf("Setting parent to non-scrolling.\n");
#endif
			if(GTK_IS_VIEWPORT(viewport)) {
				gtk_viewport_set_shadow_type(GTK_VIEWPORT(viewport),
							     GTK_SHADOW_NONE);
			}
			gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw),
						       GTK_POLICY_NEVER,
						       GTK_POLICY_NEVER);
		}
	}
}

/* This is what we do for a newline: just note that we moved to the next
 * line.  All of the resizing is handled by the table automatically. */
void parent_newline(GtkWidget *parent)
{
	int r, c;
	GtkWidget *table;
	gpointer data;

	if(parent == NULL) {
		parent = parents_peek();
	}

	/* The parent may or may not even have a table yet, so if it doesn't
	 * have one, make this a no-op. */
	data = gtk_object_get_data(GTK_OBJECT(parent), GNOME_LINUXCONF_TABLE);
	if(!GTK_IS_TABLE(data)) {
		return;
	} else {
		table = GTK_WIDGET(GTK_TABLE(data));
	}

	/* Retrive the current insertion point, increment the row, reset
	 * the column to zero, and save it back to the table. */
	r = GPOINTER_TO_INT(gtk_object_get_data(GTK_OBJECT(table),
					GNOME_LINUXCONF_CURRENT_ROW));
	c = GPOINTER_TO_INT(gtk_object_get_data(GTK_OBJECT(table),
					GNOME_LINUXCONF_CURRENT_COLUMN));
	c = 0;
	r++;
	gtk_object_set_data(GTK_OBJECT(table), GNOME_LINUXCONF_CURRENT_ROW,
			    GINT_TO_POINTER(r));
	gtk_object_set_data(GTK_OBJECT(table), GNOME_LINUXCONF_CURRENT_COLUMN,
			    GINT_TO_POINTER(c));
}
