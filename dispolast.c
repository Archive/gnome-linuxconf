/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

/* Resize the last widget that was placed to use more than one cell in its
 * parent table.  So we get the parent via the widget's new "parent" field,
 * make sure that it's a table, detach it, and then re-insert it.  Because
 * the "dispolast" command only affects the last widget, we can just
 * re-insert it with "parent_insert", which does the Right Thing for it. */
HANDLER(dispolast)
{
	GtkWidget *widget, *parent;
	GList *children;
	GtkTableChild *child;
	int c;
	GtkArg child_args[4];
	GtkArgInfo *child_info[4];

	char *horid = (char*)g_list_nth_data(args, 0);
	char *nbhcell = (char*)g_list_nth_data(args, 1);
	char *vertid = (char*)g_list_nth_data(args, 2);
	char *nbvcell = (char*)g_list_nth_data(args, 3);

	g_return_val_if_fail(horid != NULL, NULL);
	g_return_val_if_fail(nbhcell != NULL, NULL);
	g_return_val_if_fail(vertid != NULL, NULL);
	g_return_val_if_fail(nbvcell != NULL, NULL);

	if((atoi(nbhcell) == 1) && (atoi(nbvcell) == 1)) {
		return NULL;
	}

       	widget = previous_widget;
	g_assert(GTK_IS_WIDGET(widget));

	if(GTK_IS_MISC(widget)) {
		switch(horid[0]) {
			case 'c':
				gtk_misc_set_alignment(GTK_MISC(widget),
					       	       0.5, -1);
				break;
			case 'l':
				gtk_misc_set_alignment(GTK_MISC(widget),
					       	       0, -1);
				break;
			case 'r':
				gtk_misc_set_alignment(GTK_MISC(widget),
					       	       1, -1);
				break;
		}
		switch(vertid[0]) {
			case 'c':
				gtk_misc_set_alignment(GTK_MISC(widget),
					       	       -1, 0.5);
				break;
			case 't':
				gtk_misc_set_alignment(GTK_MISC(widget),
					       	       -1, 0);
				break;
			case 'b':
				gtk_misc_set_alignment(GTK_MISC(widget),
					       	       -1, 1);
				break;
		}
	}

	parent = widget->parent;
	g_return_val_if_fail(GTK_IS_TABLE(parent), NULL);

	gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_TABLE_WIDTH,
			    GINT_TO_POINTER(atoi(nbhcell)));
	gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_TABLE_HEIGHT,
			    GINT_TO_POINTER(atoi(nbvcell)));

	c = GPOINTER_TO_INT(gtk_object_get_data(GTK_OBJECT(parent),
					GNOME_LINUXCONF_CURRENT_COLUMN));
	c--;
	gtk_object_set_data(GTK_OBJECT(parent), GNOME_LINUXCONF_CURRENT_COLUMN,
			    GINT_TO_POINTER(c));

	/* Why doesn't this work? */
	memset(&child_info, 0, sizeof(child_info));
	child_args[0].name = "GtkTable::left_attach";
	gtk_container_child_arg_get_info(gtk_table_get_type(),
					 child_args[0].name,
					 &child_info[0]);
	gtk_container_arg_get(GTK_CONTAINER(parent), widget,
		       	      &child_args[0], child_info[0]);
	child_args[1].name = "GtkTable::right_attach";
	gtk_container_child_arg_get_info(gtk_table_get_type(),
					 child_args[1].name,
					 &child_info[1]);
	gtk_container_arg_get(GTK_CONTAINER(parent), widget,
		       	      &child_args[1], child_info[1]);
	child_args[2].name = "GtkTable::top_attach";
	gtk_container_child_arg_get_info(gtk_table_get_type(),
		       			 child_args[2].name,
					 &child_info[2]);
	gtk_container_arg_get(GTK_CONTAINER(parent), widget,
		       	      &child_args[2], child_info[2]);
	child_args[3].name = "GtkTable::bottom_attach";
	gtk_container_child_arg_get_info(gtk_table_get_type(),
		       			 child_args[3].name,
					 &child_info[3]);
	gtk_container_arg_get(GTK_CONTAINER(parent), widget,
		       	      &child_args[3], child_info[3]);
	switch(GTK_FUNDAMENTAL_TYPE(child_args[1].type)) {
		case GTK_TYPE_INT:
			GTK_VALUE_INT(child_args[1]) =
			GTK_VALUE_INT(child_args[0]) + atoi(nbhcell);
			break;
		case GTK_TYPE_UINT:
			GTK_VALUE_UINT(child_args[1]) =
			GTK_VALUE_UINT(child_args[0]) + atoi(nbhcell);
			break;
		case GTK_TYPE_LONG:
			GTK_VALUE_LONG(child_args[1]) =
			GTK_VALUE_LONG(child_args[0]) + atoi(nbhcell);
			break;
		case GTK_TYPE_ULONG:
			GTK_VALUE_ULONG(child_args[1]) =
			GTK_VALUE_ULONG(child_args[0]) + atoi(nbhcell);
			break;
		default: {}
	}
	switch(GTK_FUNDAMENTAL_TYPE(child_args[3].type)) {
		case GTK_TYPE_INT:
			GTK_VALUE_INT(child_args[3]) =
			GTK_VALUE_INT(child_args[2]) + atoi(nbvcell);
			break;
		case GTK_TYPE_UINT:
			GTK_VALUE_UINT(child_args[3]) =
			GTK_VALUE_UINT(child_args[2]) + atoi(nbvcell);
			break;
		case GTK_TYPE_LONG:
			GTK_VALUE_LONG(child_args[3]) =
			GTK_VALUE_LONG(child_args[2]) + atoi(nbvcell);
			break;
		case GTK_TYPE_ULONG:
			GTK_VALUE_ULONG(child_args[3]) =
			GTK_VALUE_ULONG(child_args[2]) + atoi(nbvcell);
			break;
		default: {}
	}
	gtk_container_arg_set(GTK_CONTAINER(parent), widget,
		       	      &child_args[0], child_info[0]);
	gtk_container_arg_set(GTK_CONTAINER(parent), widget,
		       	      &child_args[1], child_info[1]);
	gtk_container_arg_set(GTK_CONTAINER(parent), widget,
		       	      &child_args[2], child_info[2]);
	gtk_container_arg_set(GTK_CONTAINER(parent), widget,
		       	      &child_args[3], child_info[3]);
	gtk_widget_hide(parent);
	gtk_widget_hide(widget);
	gtk_widget_queue_resize(widget);
	gtk_widget_show_all(parent);

	return NULL;
}
