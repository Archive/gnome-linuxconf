/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

HANDLER(version)
{
	/* We support version 3 of the GUI protocol. */
	g_assert(atoi((char*)g_list_nth_data(args, 0)) == 3);
	return NULL;
}
