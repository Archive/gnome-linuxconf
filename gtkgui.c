/*
 * A GNOME front end for Linuxconf
 *
 * Copyright (C) 1997 the Free Software Foundation
 * Copyright (C) 1998, 1999 Red Hat Software, Inc.
 *
 * Authors: Miguel de Icaza (miguel@gnu.org)
 *          Michael K. Johnson <johnsonm@redhat.com>
 *
 */
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>
#include <gnome.h>
#include "io.h"
#include "parse.h"

/*  Organization

This remadmin protocol display engine works roughly like this:

initialize parser and io engines
read a line of input
pass the line to the parser
if the parser returns a command value:
	if the command is not a mainform:
		fail
	pass the command to the mainform drawing routine

Note: all first-class widget drawing routines (as opposed to helper
routines) return an unplaced widget to their caller, on the assumption
that the parent will place that widget.  The exception is the mainform
drawing routine, which creates a toplevel instead.

The mainform drawing routine creates the toplevel and then creates a
virtual subform (table) within itself by calling the form drawing routine.
The form drawing routine returns the table which mainform places within
itself, knowing nothing more about it.

The form drawing routine creates a table and iterates over its children.
for each child:
	if it is a widget:
		get a widget from the appropriate widget-creation routine
		place it in the table using current and widget constraints
	if it is not a widget:
		do the right thing to the state for the next widget

All current disposition state (current column, line, etc) is kept in
local variables in the form drawing routine.

The book drawing routine iterates over its children to make sure that
they are all pages, and creates pages with proper tab names, whose
children are forms gotten from the form drawing routine.

The formbutton drawing routine creates an hbox with proper expansion
and places its children itself, like the form drawing routine does.

The group[fit] drawing routine creates a gtk frame and then calls through
to the form drawing routine to place a form inside the grame.

create_widget is a dispatcher to all the more specific widget creation
routines.  It does this based on the widget type.  It returns the return
value of whatever function it calls.

The widget_info structures always need to be bound to their associated gtk
widgets, and the gtk widgets always need to be bound to their associated
widget_info structures.  The first is used for deleting gtk widget trees
when we are done with them (because that is initiated by the parser
from a delete command, and the parser doesn't know what a gtk widget is)
and the second is used to find the data that goes with a widget when an
action is detected (like a button click).

When there could be two different places that this correspondence is
maintained (for instance, is the mainform gtk widget the toplevel or
the table within it?), it will always be set to be the higher widget
in the gtk hierarchy, to eliminate confusion and make the delete
operator work.  Choice menu items in a single group all have the same
widget_info bound to them so they can stuff the value in when they
are activated.


IFF treemenu support is being used, then there is a concept of the
main window.  It gets created when the application is started, and
when the tree is sent, the tree itself is identified with the window
logically, so that when the tree is deleted, the whole application
quits.  When treemenu support is in place, new windows show up as notebook
tabs within the main window.  The main logical change is that with
treemenu support in place, everything gets added to an automatic
scroll window so we don't have to have the scroll window hacks that
we have been using.

It is not clear yet if help text should show up in tabs or in separate
windows.  Once we use GNOME MDI (where we can drag tabs out and put
them in separate windows) the point may become moot...

*/

typedef enum hints_e {
	EXPAND = 1<<0,	/* this table element should expand */
	WIDE = 1<<1,	/* make this window wider */
} hints;


/* The widget_info structure extends the command structure from parse.h */
typedef struct widget_info_s {
	/* command */
	command_type	type;
	disposition	*disp;		/* may be NULL */
	char		*id;
	char		*str;		/* may be NULL */
	void		*data;		/* may be NULL */
	int		idata;
	void		*ref;		/* may be NULL */
	char		*path;
	command		*parent;	/* may be NULL */
	command		*form;		/* MAIN form   */
	GSList		*child_list;	/* may be NULL */
	/* gtk-specific stuff */
	GtkWidget *gtkwidget;
	void		*state; 	/* state storage */
	hints		hint;		/* hints set before creating form */
	char		*treepath;	/* tree path used to invoke widget */
} widget_info;



/* file-scope variables */
static ios *io;
static guint input_tag;
static char *widget_info_key = "lc_widget_info";
static widget_info *mainwidget = NULL;
static GSList *pagelist = NULL;
static GHashTable *showing_tabs;
static char *treepath = NULL;
static GtkWidget *focus_widget = NULL;
static int tree_id_num = 0;
static GnomeApp *app = NULL;

/* forward declarations */
GtkWidget* create_widget (widget_info *w);

/* functions used by the parser interface */
command *
commandAlloc (void) {
	return g_malloc0(sizeof(widget_info));
}

/* No need for a command_free function, because the GtkWidget reference
 * will go away on the commandDeleteTree call.  This will change if we
 * add any data items to widget_info that need to be freed.
 */

void
commandDeleteTree(command *c) {
	widget_info *w = (widget_info*) c;
	gchar *path;

	gtk_widget_destroy (w->gtkwidget);
	if (pagelist) {
		pagelist = g_slist_remove(pagelist, w);
	}

	if (w->treepath) {
		g_hash_table_remove(showing_tabs, w->treepath);
		g_free(w->treepath);
	}
}


int
protocolCheck (int version, int good_so_far, char *bad_message) {

	if ((version != PROTOCOL_VERSION) || (!good_so_far)) {
		GtkWidget *dialog;
		GtkWidget *label;
		GList *buttons;

		dialog = gnome_dialog_new(_("Warning"),
					  GNOME_STOCK_BUTTON_CLOSE,
					  NULL);

		label = gtk_label_new(bad_message);
		gtk_box_pack_start(GTK_BOX(app->vbox), label,
				   FALSE, FALSE, GNOME_PAD_SMALL);
		gtk_widget_show(label);

		buttons = (GNOME_DIALOG(dialog))->buttons;
		gtk_signal_connect(GTK_OBJECT(buttons->data), "clicked",
				   GTK_SIGNAL_FUNC(gtk_main_quit), NULL);

		gnome_dialog_run_and_close(GNOME_DIALOG(dialog));
	}
	return TRUE;
}

/* gtk helper functions */
static widget_info *
get_widget_info(GtkWidget *widget) {
	if(GTK_IS_WIDGET(widget)) {
		return gtk_object_get_data(GTK_OBJECT(widget), widget_info_key);
	} else {
		return NULL;
	}
}

static void
set_widget_info(GtkWidget *widget, widget_info *w) {
	gtk_object_set_data(GTK_OBJECT(widget), widget_info_key, w);
}

static void
bind_widget_info(GtkWidget *widget, widget_info *w) {
	w->gtkwidget = widget;
	set_widget_info(widget, w);
}

static float
h_align(disp_horiz d) {
	switch (d) {
	case H_LEFT: return 0.0;
	case H_RIGHT: return 1.0;
	case H_CENTER:
	default:
		return 0.5;
	}
}

static float
v_align(disp_vert d) {
	switch (d) {
	case V_TOP: return 0.0;
	case V_BOTTOM: return 1.0;
	case V_CENTER:
	default:
		return 0.5;
	}
}

static void
widget_do_dump_tree(widget_info *w) {
	/* if this widget is dumpable, print its value */
	switch (w->type) {
	case C_CHECKBOX:
		io_write_message ("dump %s %s %d\n", w->path, w->id,
			      GTK_TOGGLE_BUTTON(w->gtkwidget)->active);
		break;
	case C_CHOICE:
		if (w->state)
			io_write_message ("dump %s %s %s\n", w->path, w->id,
					(char *) w->state);
		break;
	case C_COMBO:
		io_write_message ("dump %s %s %s\n", w->path, w->id,
			gtk_entry_get_text(GTK_ENTRY(
				(GTK_COMBO(w->gtkwidget))->entry)));
		break;
	case C_STRING:
	case C_PASSWORD:
		io_write_message ("dump %s %s %s\n", w->path, w->id,
			      gtk_entry_get_text(GTK_ENTRY(w->gtkwidget)));
		break;
	case C_RADIO:
		/* only print the value of the currently-selected button */
		if (GTK_TOGGLE_BUTTON(w->gtkwidget)->active) {
			widget_info *group = w->ref;

			io_write_message ("dump %s %s %s\n", w->path,
				group->id, (char *) w->data);
		}
		break;
	case C_TREESUB: {
		tree_item_data *item = w->data;
		if (item->expand) {
			char *tpath = parse_get_tree_path((command *)w);
			io_write_message("dump %s t%d %s\n",
					 w->id, tree_id_num++, tpath);
			break;
		}
	}
	default:
		/* not a printable item */
		break;
	}

	/* recurse */
	g_slist_foreach (w->child_list, (GFunc) widget_do_dump_tree, NULL);
}

static void
widget_dump_tree(widget_info *w) {
	tree_id_num = 0;
	widget_do_dump_tree(w);
}

static void
report_action (GtkWidget *widget, gpointer data) {
	widget_info *w = get_widget_info(widget);

	io_write_message ("action %s %s\n", w->path, w->id);
}

static void
close_action (GtkWidget *widget, gpointer data) {
	widget_info *w = get_widget_info(widget);

	if (!w) return;
	widget_dump_tree(w);
	io_write_message ("action %s X\n", w->path ? w->path : w->id );
}

static void
button_dump_form (GtkWidget *widget, gpointer data) {
	widget_info *w = get_widget_info(widget);

	if (!w) return;
	widget_dump_tree((widget_info *)w->form);
	report_action (widget, data);
}

static void
subtree_expand (GtkWidget *widget, gpointer data) {
	widget_info *w = get_widget_info(widget);
	tree_item_data *item = w->data;

	item->expand = (int) data; /* we know data will be 0 or 1 */
}

static void
entry_enter_pressed (GtkWidget *widget, gpointer data) {
	mainform_data *md;
	widget_info *w = get_widget_info(widget);

	if (!w || !w->form || !w->form->data) return;

	md = w->form->data;
	widget_dump_tree((widget_info *)w->form);
	io_write_message ("action %s %s\n", w->path, md->enteraction);
}

static void
generic_button_setup (GtkWidget *button, int should_dump) {
	if (should_dump)
		gtk_signal_connect (GTK_OBJECT(button), "clicked",
				    GTK_SIGNAL_FUNC(button_dump_form), 0);
	else
		gtk_signal_connect (GTK_OBJECT(button), "clicked",
				    GTK_SIGNAL_FUNC(report_action), 0);
}


void
set_choice_value (GtkWidget *widget, char *value)
{
	widget_info *w = get_widget_info(widget);

	w->state = value;
}


void
select_clist_row (GtkWidget *widget, gint row)
{
	clist_data *cd;
	clist_item *ci;
	widget_info *w = get_widget_info(widget);

	cd = w->data;
	ci = g_slist_nth(cd->itemlist, row)->data;

	io_write_message ("action %s %s\n", w->path, ci->id);
}

void
select_tree_item (GtkWidget *root_tree, GtkWidget *child, GtkWidget *subtree)
{
	widget_info *w = get_widget_info(child);

	gtk_tree_unselect_child(GTK_TREE(root_tree), child);
	if (!w || w->type == C_TREESUB) return;

	treepath = parse_get_tree_path((command *)w);

	if (g_hash_table_lookup(showing_tabs, treepath)) {
		g_free(treepath);
		return;
	}

	g_hash_table_insert(showing_tabs, treepath, w);
	io_write_message ("action %s %s\n", w->path, treepath);
	/* treepath freed from g_hash_table_remove() */
}

/* protocol-related widget creation functions */

GtkWidget *
create_table(widget_info *w) {
	GtkWidget *table;
	GtkWidget *scrolled_window;
	GtkWidget *align;
	int column = 0, row = 0;
	GSList *child_list;

	if (!w->child_list) {
		return NULL;
	}

	table = gtk_table_new(0, 0, FALSE);
	gtk_container_set_border_width(GTK_CONTAINER(table), GNOME_PAD_SMALL);
	if (!w->gtkwidget) {
		/* only set if parent didn't set already */
		bind_widget_info(table, w);
	}

	for (child_list = w->child_list;
	     child_list != NULL;
	     child_list = g_slist_next(child_list)) {
		GtkWidget *current_child;
		GtkTable *t = GTK_TABLE(table);
		widget_info *child;
		gint fill_options;

		/* muck with column to take into account any spans */
		if (t->children) {
			GList *cl = t->children;
			GtkTableChild *c;
			
			for (c = cl->data; cl; cl = g_list_next(cl)) {
				/* if the current location is within
				 * a spanned area, make the current
				 * column be the right edge of the area
				 */
				if ((row >= c->top_attach) &&
				    (row < c->bottom_attach) &&
				    (column >= c->left_attach) &&
				    (column < c->right_attach)) {
					column = c->right_attach;
					break;
				}
			}
		}

		child = child_list->data;

		/* handle non-widget children */
		switch (child->type) {
			case C_NEWLINE:
				column = 0;
				row++;
				continue;
			case C_SKIP:
				column += child->idata;
				continue;
			default:
				break;
		}

		/* at this point, we are sure we should create a widget */
		current_child = create_widget(child);

		if (child->hint & EXPAND)
			fill_options = GTK_SHRINK | GTK_FILL | GTK_EXPAND;
		else
			fill_options = GTK_SHRINK | GTK_FILL;

		/* now place the child */
		if (child->disp) {
			/* alignment is specified */
			align = gtk_alignment_new (
				h_align(child->disp->horiz),
				v_align(child->disp->vert),
				1.0, 1.0);
			gtk_widget_show(align);

			gtk_container_add(GTK_CONTAINER(align), current_child);

			gtk_table_attach(GTK_TABLE(table), align,
				 	column, column+child->disp->num_horiz,
				 	row, row+child->disp->num_vert,
				 	fill_options, fill_options,
				 	GNOME_PAD_SMALL, 0);

			column = column+child->disp->num_horiz;
		} else {
			/* use default alignment */
			align = gtk_alignment_new (
				h_align(H_LEFT),
				v_align(V_CENTER),
				1.0, 1.0);
			gtk_widget_show(align);

			gtk_container_add (GTK_CONTAINER(align), current_child);

			gtk_table_attach(GTK_TABLE(table), align,
				 	column, column + 1,
					row, row + 1,
				 	fill_options, fill_options,
				 	GNOME_PAD_SMALL, 0);
			column++;
		}
	}

	/* see whether we need scrollbar */

	if (row > 14) { /* this measure is a HACK */
		scrolled_window = gtk_scrolled_window_new (NULL, NULL);
		/* horizontal should be GTK_POLICY_NEVER if it existed */
		gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_window),
					       GTK_POLICY_AUTOMATIC,
					       GTK_POLICY_AUTOMATIC);
#ifdef GTK_HAVE_FEATURES_1_1_4
		gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrolled_window),
						      table);
#else
		gtk_container_add(GTK_CONTAINER(scrolled_window), table);
#endif
		/* the next line is a hack until we can get better
		 * size negotiation
		 */
		if(w->hint && WIDE) {
			gtk_widget_set_usize(scrolled_window, 470, 400);
		} else {
			gtk_widget_set_usize(scrolled_window, 380, 400);
		}
		gtk_widget_show_all(scrolled_window);
		return scrolled_window;
	} else {
		gtk_widget_show_all(table);
		return table;
	}
}

GtkWidget *
create_hline(widget_info *w) {
	GtkWidget *sep;

	if (w->str) {
		GtkWidget *wtmp;
		sep = gtk_hbox_new(FALSE, GNOME_PAD_SMALL);
		gtk_box_pack_start(GTK_BOX(sep), wtmp = gtk_hseparator_new(),
				   FALSE, FALSE, GNOME_PAD_SMALL);
		gtk_widget_show(wtmp); /* Keep from flashing */
		gtk_box_pack_start(GTK_BOX(sep), wtmp = gtk_label_new(w->str),
				   FALSE, FALSE, GNOME_PAD_SMALL);
		gtk_widget_show(wtmp); /* Keep from flashing */
		gtk_box_pack_start(GTK_BOX(sep), wtmp = gtk_hseparator_new(),
				   FALSE, FALSE, GNOME_PAD_SMALL);
		gtk_widget_show(wtmp); /* Keep from flashing */
	} else {
		sep = gtk_hseparator_new ();
		gtk_widget_show(sep); /* Keep from flashing */
	}
	bind_widget_info(sep, w);

	return sep;
}

GtkWidget *
create_label (widget_info *w) {
	GtkWidget *label;

	label = gtk_label_new (w->str);
	gtk_misc_set_alignment (GTK_MISC (label),
				h_align(H_LEFT), v_align(V_CENTER));
	bind_widget_info(label, w);
	gtk_widget_show(label); /* Keep from flashing */

	return label;
}

GtkWidget *
create_entry (widget_info *w, int visible)
{
	GtkWidget *entry;
	
	entry = gtk_entry_new ();
	/* hmm, maxlen max visible length in characters doesn't mean much...
	 * If we come up with something for it to mean, the length is in
	 * w->idata for us to use.
	 */
	gtk_entry_set_visibility(GTK_ENTRY(entry), visible);
	if (w->str)
		gtk_entry_set_text (GTK_ENTRY(entry), w->str);
	bind_widget_info (entry, w);
	if (w->form && w->form->data) {
		mainform_data *md;

		md = w->form->data;
		if (md->enteraction) {
			gtk_signal_connect (GTK_OBJECT(entry), "activate",
			    GTK_SIGNAL_FUNC(entry_enter_pressed), 0);
			focus_widget = entry;
		}
	}

	gtk_widget_show(entry);
	return entry;
}

GtkWidget *
create_checkbox (widget_info *w) {
	GtkWidget *check;

	check = gtk_check_button_new_with_label (w->str);
	gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(check),
				    w->idata == CB_ON ? TRUE : FALSE);
	bind_widget_info(check, w);
	gtk_widget_show(check);

	return check;
}

GtkWidget *
create_radio (widget_info *w) {
	GtkWidget *radio, *rb;
	GSList *rbg = NULL;
	widget_info *group = w->ref;

	if (group == w) {
		/* this starts a new radio group */
		radio = gtk_radio_button_new_with_label(NULL, w->str);
	} else {
		/* this is part of a radio group */
		rb = GTK_WIDGET(GTK_RADIO_BUTTON(group->gtkwidget));
		rbg = gtk_radio_button_group(GTK_RADIO_BUTTON(rb));
		radio = gtk_radio_button_new_with_label(rbg, w->str);
	}
	gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(radio), w->idata);
	bind_widget_info (radio, w);
	gtk_widget_show(radio);

	return radio;
}

GtkWidget *
create_button (widget_info *w, int should_dump, int should_fill) {
	GtkWidget *button;
	GtkWidget *label;

	/* FIXME: should_fill should be converted to information that
	 * can be used while packing this in by its parent container.
	 */

	/* cannot use gtk_button_new_with_label because remadmin
	 * protocol wants left justification by default
	 */

	button = gtk_button_new();
	label = gtk_label_new(w->str);
	gtk_widget_show(label); /* Keep from flashing */
	gtk_misc_set_alignment (GTK_MISC (label),
				h_align(H_LEFT), v_align(V_CENTER));
	gtk_misc_set_padding(GTK_MISC(label), GNOME_PAD_SMALL, 0);
	gtk_container_add (GTK_CONTAINER (button), label);
	generic_button_setup(button, should_dump);
	bind_widget_info (button, w);
	gtk_widget_show(button);

	return button;
}

GtkWidget *
create_button_xpm (widget_info *w, int is_string) {
	GtkWidget *button, *pixmap, *box, *label;

	button = gtk_button_new ();

	if (is_string) {
		char **s = w->data;
		pixmap = gnome_pixmap_new_from_xpm_d (s);
	} else {
		pixmap = gnome_pixmap_new_from_file(w->data);
	}

	if (w->str){
		/* We have text */
		box = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);
		label = gtk_label_new(w->str);
		gtk_widget_show(label); /* Keep from flashing */
		gtk_box_pack_start(GTK_BOX(box), pixmap, FALSE, FALSE,
				   GNOME_PAD_SMALL);
		gtk_box_pack_start(GTK_BOX(box), label, FALSE, FALSE,
				   GNOME_PAD_SMALL);
		gtk_container_add (GTK_CONTAINER(button), box);
	} else {
		label = NULL;
		box = NULL;
		gtk_container_add (GTK_CONTAINER(button), pixmap);
	}

	generic_button_setup (button, 0);
	bind_widget_info (button, w);
	gtk_widget_show(button);

	return button;
}

GtkWidget *
create_choice (widget_info *w) {
	GtkWidget *choice, *menu;
	GSList *l;
	int i;

	choice = gtk_option_menu_new ();
	menu = gtk_menu_new ();
	gtk_option_menu_set_menu(GTK_OPTION_MENU (choice), menu);
	gtk_widget_show(menu);
	w->state = w->str;

	for (i=0, l = w->data; l; i++, l = g_slist_next(l)) {
		GtkWidget *item;
		combo_item *ci;

		ci = l->data;
		item = gtk_menu_item_new_with_label(ci->value);
		gtk_signal_connect(GTK_OBJECT(item), "activate",
				   GTK_SIGNAL_FUNC(set_choice_value),
				   ci->value);
		gtk_menu_append (GTK_MENU(menu), item);
		/* note: special case: widget_info not 1-1 with GtkWidget */
		set_widget_info (item, w);
		gtk_widget_show(item);

		if (!strcmp(w->str, ci->value)) {
			gtk_option_menu_set_history(GTK_OPTION_MENU(choice), i);
		}
	}

	bind_widget_info (choice, w);
	gtk_widget_show(choice);

	return choice;
}

GtkWidget *
create_clist (widget_info *w) {
	GtkWidget *clist;
#ifdef GTK_HAVE_FEATURES_1_1_4
	GtkWidget *clist_scroller;
#endif
	clist_data *cd;
	int *widths, totalwidth = 0;
	GSList *itemlist;
	int cols, i, showrows;

	cd = w->data;
	cols = w->idata;

	widths = g_malloc0((cols + 1) * sizeof (int));

	/* clean all this up when Gtk 1.0 deprecated */
#ifdef GTK_HAVE_FEATURES_1_1_2
	/* closest I can come to a #define for the new clist interface :-( */
	clist = gtk_clist_new_with_titles (cols, cd->titles);
	clist_scroller = gtk_scrolled_window_new (NULL, NULL);
	gtk_container_add (GTK_CONTAINER(clist_scroller), clist);
	gtk_widget_show(clist);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(clist_scroller),
				       GTK_POLICY_AUTOMATIC,
				       GTK_POLICY_AUTOMATIC);
#else
	clist = gtk_clist_new_with_titles (cols, cd->titles);
	gtk_clist_set_policy(GTK_CLIST(clist),
			     GTK_POLICY_AUTOMATIC,
			     GTK_POLICY_AUTOMATIC);
#endif
	gtk_clist_set_selection_mode (GTK_CLIST(clist), GTK_SELECTION_SINGLE);
	gtk_signal_connect_after (GTK_OBJECT (clist), "select_row",
				  GTK_SIGNAL_FUNC(select_clist_row), NULL);

	/* get maximum width for each list, figure out how tall to make it */
	for (showrows = 0, itemlist = cd->itemlist;
	     itemlist;
	     itemlist = g_slist_next(itemlist)) {
		clist_item *ci = itemlist->data;

		for (i = 0; i < cols; i++) {
			int this_width;

			this_width = gdk_string_width(clist->style->font,
					ci->values[i]);
			widths[i] = this_width > widths[i] ?
					this_width :
					widths[i];
		}

		if (showrows < 10) showrows++;
	}

	for (i = 0; i < cols; i++) {
		int this_width;
		/* need to account for title width, too */
		this_width = gdk_string_width(clist->style->font,
				cd->titles[i])
			     + 8; /* fudge for decoration widths */
		widths[i] = this_width > widths[i] ?
				this_width :
				widths[i];

		gtk_clist_column_title_passive (GTK_CLIST(clist), i);
		gtk_clist_set_column_width(GTK_CLIST(clist), i, widths[i]);
		totalwidth += widths[i];
	}

	/* guess at the needed length, amend this as necessary... */
	gtk_widget_set_usize(clist, totalwidth + (cols*14) + 5,
			     (showrows*20) + 40);

	/* now fill in the clist */
	gtk_clist_freeze(GTK_CLIST(clist));
	for (itemlist = cd->itemlist;
	     itemlist;
	     itemlist = g_slist_next(itemlist)) {
		clist_item *ci = itemlist->data;

		gtk_clist_append(GTK_CLIST(clist), ci->values);
	}
	gtk_clist_thaw(GTK_CLIST(clist));

	bind_widget_info (clist, w);

#ifdef GTK_HAVE_FEATURES_1_1_2
	gtk_widget_show(clist_scroller);
	return clist_scroller;
#else
	gtk_widget_show(clist);
	return clist;
#endif
}

GtkWidget *
create_combo (widget_info *w) {
	GtkWidget *combo;
	GList *list = NULL;
	GSList *cilist = NULL;

	/* FIXME: linuxconf wants more than a standard combobox here */

	combo = gtk_combo_new ();

	for (cilist = w->data; cilist; cilist = g_slist_next(cilist)) {
		combo_item *ci = cilist->data;

		/* ignore comments for now... */
		list = g_list_append(list, ci->value);

	}
	gtk_combo_set_popdown_strings(GTK_COMBO(combo), list);

	gtk_entry_set_text (GTK_ENTRY((GTK_COMBO(combo))->entry), w->str);
	bind_widget_info (combo, w);
	gtk_widget_show(combo);

	return combo;
}

GtkWidget *
create_group (widget_info *w) {
	GtkWidget *group, *table;

	group = gtk_frame_new (w->str);
	gtk_container_set_border_width(GTK_CONTAINER(group), GNOME_PAD_SMALL);
	bind_widget_info(group, w);
	gtk_widget_show(group);

	table = create_table(w);
	gtk_container_add(GTK_CONTAINER(group), table);
	gtk_widget_show(table);

	return group;
}

GtkWidget *
create_book (widget_info *w) {
	GtkWidget *book;
	GSList *child_list;


	book = gtk_notebook_new ();
	gtk_container_set_border_width(GTK_CONTAINER(book), GNOME_PAD_SMALL);
	bind_widget_info (book, w);

	for (child_list = w->child_list;
	     child_list;
	     child_list = g_slist_next(child_list)) {
		widget_info *wc = child_list->data;
		GtkWidget *table, *label;

		/* the only children a book can have are pages */
		if (wc->type != C_PAGE) continue;
	
		label = gtk_label_new (wc->str);
		gtk_widget_show(label); /* Keep from flashing */
		table = create_table (wc);
		bind_widget_info(table, wc);
		gtk_notebook_append_page (GTK_NOTEBOOK(book), table, label);
	}

	gtk_widget_show(book);
	return book;
}

GtkWidget *
create_form_button (widget_info *w) {
	GtkWidget *form;
	GSList *child_list;

	form = gtk_hbox_new(FALSE, GNOME_PAD_SMALL);
	gtk_container_border_width(GTK_CONTAINER(form), GNOME_PAD_SMALL);
	bind_widget_info(form, w);

	for (child_list = w->child_list;
	     child_list;
	     child_list = g_slist_next(child_list)) {
		widget_info *child = child_list->data;
		GtkWidget *current_child;

		current_child = create_widget(child);
		gtk_box_pack_start(GTK_BOX(form), current_child, FALSE, FALSE,
				   GNOME_PAD_SMALL);
	}

	gtk_widget_show(form);
	return form;
}

void create_menutree (widget_info *w, GtkWidget *tree);

void
create_treeitem(widget_info *w, GtkWidget *p) {
	tree_item_data *item;
	GtkWidget *treeitem;

	item = w->data;
	if (item->icon_xpm) {
		GtkWidget *hbox, *pixmap, *label;

		pixmap = gnome_pixmap_new_from_xpm_d (item->icon_xpm);
		label = gtk_label_new (w->str);
		gtk_widget_show(label); /* Keep from flashing */
		gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
		hbox = gtk_hbox_new(FALSE, GNOME_PAD_SMALL);
		gtk_box_pack_start(GTK_BOX(hbox), pixmap, FALSE, FALSE,
				   GNOME_PAD_SMALL);
		gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE,
				   GNOME_PAD_SMALL);
		treeitem = gtk_tree_item_new ();
		gtk_container_add (GTK_CONTAINER (treeitem), hbox);
		gtk_widget_show(hbox);
	} else {
		treeitem = gtk_tree_item_new_with_label(w->str);
	}

	if (w->type == C_TREESUB) {
		GtkWidget *subtree;

		subtree = gtk_tree_new();
		gtk_tree_append (GTK_TREE(p), treeitem);
		gtk_widget_show(treeitem);
		gtk_tree_item_set_subtree(GTK_TREE_ITEM(treeitem), subtree);
		gtk_widget_show(subtree);
		create_menutree (w, subtree);
		if (item->expand)
			gtk_tree_item_expand(GTK_TREE_ITEM(treeitem));
		else
			gtk_tree_item_collapse(GTK_TREE_ITEM(treeitem));
		gtk_signal_connect(GTK_OBJECT(treeitem), "expand",
			GTK_SIGNAL_FUNC(subtree_expand), GINT_TO_POINTER(1));
		gtk_signal_connect(GTK_OBJECT(treeitem), "collapse",
			GTK_SIGNAL_FUNC(subtree_expand), GINT_TO_POINTER(0));
	} else {
		gtk_tree_append (GTK_TREE(p), treeitem);
	}
	bind_widget_info(treeitem, w);
	gtk_widget_show (treeitem);
}

void
create_menutree (widget_info *w, GtkWidget *tree) {
	GSList *child_list;

	bind_widget_info(tree, w);
	gtk_tree_set_selection_mode (GTK_TREE(tree), GTK_SELECTION_BROWSE);
	gtk_signal_connect (GTK_OBJECT(tree), "select_child",
			    GTK_SIGNAL_FUNC(select_tree_item), tree);

	for (child_list = w->child_list;
	     child_list;
	     child_list = g_slist_next(child_list)) {
		widget_info *child = child_list->data;
		create_treeitem(child, tree);
	}
}

GtkWidget *
create_tree_menu (widget_info *w) {
	GtkWidget *tree;

	tree = gtk_tree_new();
	create_menutree (w, tree);
	gtk_widget_show(tree);

	return tree;
}


/* dispatcher */
GtkWidget *
create_widget (widget_info *w) {
	switch (w->type) {
	case C_FORM:
		return create_table(w);
	case C_HLINE:
		return create_hline(w);
	case C_LABEL:
		return create_label(w);
	case C_STRING:
		return create_entry(w, 1);
	case C_PASSWORD:
		return create_entry(w, 0);
	case C_CHECKBOX:
		return create_checkbox(w);
	case C_RADIO:
		return create_radio(w);
	case C_BUTTON_DUMP:
		return create_button(w, TRUE, FALSE);
	case C_BUTTON:
		return create_button(w, FALSE, FALSE);
	case C_BUTTON_FILL:
		return create_button(w, FALSE, TRUE);
	case C_BUTTON_XPM:
		return create_button_xpm(w, 1);
	case C_BUTTON_XPMF:
		return create_button_xpm(w, 0);
	case C_CHOICE:
		return create_choice(w);
	case C_CLIST:
		return create_clist(w);
	case C_COMBO:
		return create_combo(w);
	case C_GROUP:
		return create_group(w);
	case C_BOOK:
		return create_book(w);
	case C_FORM_BUTTON:
		return create_form_button(w);
	case C_TREEMENU:
		return create_tree_menu(w);
	default:
		/* g_assert_not_reached(); */
		return NULL;
	}
}

void
create_mainform(widget_info *w)
{
	GtkWidget *table;
	static GtkWidget *treetop = NULL;
	static GtkWidget *pagetop = NULL;
	static GtkWidget *buttonbox = NULL;
	static GtkWidget *pane = NULL;
	static GtkWidget *vbox = NULL;
	widget_info *c;

	GnomeUIInfo file_menu[] = {
		GNOMEUIINFO_MENU_EXIT_ITEM(close_action, NULL),
		GNOMEUIINFO_END,
	};
	GnomeUIInfo help_menu[] = {
		GNOMEUIINFO_END,
	};
	GnomeUIInfo main_menu[] = {
		GNOMEUIINFO_MENU_FILE_TREE(file_menu),
		GNOMEUIINFO_MENU_HELP_TREE(help_menu),
		GNOMEUIINFO_END,
	};

	if(w->child_list) {
		c = w->child_list->data;
	}

	if((io->flags & P_treemenu) && !pagetop && 
	   c && (c->type == C_TREEMENU)) {
		app = GNOME_APP(gnome_app_new(PACKAGE, VERSION));
		/* gnome_app_create_menus(app, main_menu); */
		gtk_window_set_title(GTK_WINDOW(app), _("GNOME-Linuxconf"));

		set_widget_info(GTK_WIDGET(app), w);
		gtk_signal_connect(GTK_OBJECT(app), "destroy",
				   GTK_SIGNAL_FUNC(close_action), NULL);
		gtk_signal_connect(GTK_OBJECT(app), "delete_event",
				   GTK_SIGNAL_FUNC(close_action), NULL);

		vbox = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);
		gnome_app_set_contents(app, vbox);
		gtk_widget_show(vbox);

		pane = gtk_hpaned_new();
		gtk_container_set_border_width(GTK_CONTAINER(pane),
					       GNOME_PAD_SMALL);
		gtk_box_pack_start(GTK_BOX(vbox), pane, FALSE, FALSE,
				   GNOME_PAD_SMALL);
		gtk_widget_show(pane);

		buttonbox = gtk_hbox_new(FALSE, GNOME_PAD_SMALL);
		gtk_widget_set_usize(buttonbox, 600, 35);
		gtk_box_pack_start(GTK_BOX(vbox), buttonbox, FALSE, FALSE, 0);
		gtk_widget_show(buttonbox);

		treetop = gtk_scrolled_window_new (NULL, NULL);
		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW(treetop),
						GTK_POLICY_AUTOMATIC,
						GTK_POLICY_AUTOMATIC);
		gtk_widget_set_usize(treetop, 320, 320);
		gtk_paned_pack1(GTK_PANED(pane), treetop, FALSE, FALSE);
		gtk_widget_show(treetop);

		pagetop = gtk_notebook_new();
		gtk_container_set_border_width(GTK_CONTAINER(pagetop),
					       GNOME_PAD_SMALL);
		gtk_notebook_set_scrollable (GTK_NOTEBOOK(pagetop), TRUE);
		gtk_paned_pack2(GTK_PANED(pane), pagetop, FALSE, FALSE);
		gtk_widget_show(pagetop);

		gtk_paned_set_position(GTK_PANED(pane), 320);
	}

	if (pagetop && !(w->hint & EXPAND) && !(w->hint & WIDE)) {
		if (w->child_list && (c->type == C_TREEMENU)) {
			/* If the first thing we get is a tree menu, make the
			 * tree menu the main element on the left. */
			GtkWidget *tree;
			GtkWidget *tmp;
			tree = create_tree_menu(c);
#ifdef GTK_HAVE_FEATURES_1_1_4
			gtk_scrolled_window_add_with_viewport (
				GTK_SCROLLED_WINDOW(treetop), tree);
#else
			gtk_container_add(GTK_CONTAINER (treetop), tree);
#endif
			gtk_widget_show(tree);

			/* next is newline, next->next is buttonbox */
			c = w->child_list->next->next->data;
			gtk_box_pack_start(GTK_BOX(buttonbox),
					   tmp = create_table(c),
					   FALSE, FALSE, GNOME_PAD_SMALL);
			gtk_table_set_homogeneous(GTK_TABLE(tmp), FALSE);

			mainwidget = w;
			w->gtkwidget = GTK_WIDGET(app);
			gtk_widget_show_all(GTK_WIDGET(app));
		} else {
			GtkWidget *tmp;

			table = create_table(w);
			w->treepath = treepath;
			treepath = NULL;
			bind_widget_info(table, w);
			gtk_widget_show(table);

			gtk_notebook_append_page(GTK_NOTEBOOK(pagetop), table,
						 tmp = gtk_label_new(w->str));
			gtk_widget_show(tmp);

			pagelist = g_slist_append(pagelist, w);
			gtk_notebook_set_page(GTK_NOTEBOOK(pagetop),
					      g_slist_length(pagelist) - 1);
			if(focus_widget) {
				gtk_widget_grab_focus(focus_widget);
				focus_widget = NULL;
			}
		}
	} else {
		/* old-style toplevel window
		 * if help window or if treemenu not in use
		 * or if treemenu widget has not yet been sighted
		 */
		app = GNOME_APP(gnome_app_new(PACKAGE, VERSION));
		// gnome_app_create_menus(app, main_menu);
		gtk_window_set_title(GTK_WINDOW(app), _("GNOME-Linuxconf"));
		gtk_signal_connect(GTK_OBJECT(app), "destroy",
				   GTK_SIGNAL_FUNC(close_action), NULL);
		gtk_signal_connect(GTK_OBJECT(app), "delete_event",
				   GTK_SIGNAL_FUNC(close_action), NULL);
		bind_widget_info(GTK_WIDGET(app), w);

		table = create_table(w);  /* this creates the WHOLE tree */
		gtk_window_set_title(GTK_WINDOW(app), w->str);
		gnome_app_set_contents(app, table);
		gtk_widget_show(table);

		if(focus_widget) {
			gtk_widget_grab_focus(focus_widget);
			focus_widget = NULL;
		}

		gtk_widget_show_all(GTK_WIDGET(app));
	}
}

/* add a hint or set of hints to a whole tree */
void
hint_tree (widget_info *w, hints *h)
{
	w->hint |= *h;
	g_slist_foreach(w->child_list, (GFunc)hint_tree, h);
}

/* currently only hint help pages; if we want to hint something else,
 * break this out into a hint_help function and call all hint functions
 * in turn.
 */
void
hint_form (widget_info *w)
{
	widget_info *child;
	widget_info *group_child;
	GSList *child_list;
	GSList *group_child_list;
	hints hint;

	child_list = w->child_list;
	child = child_list->data;
	if(child->type != C_GROUP) {
		return;
	}

	/* save the group list to look at later, do the cheap checks first */
	group_child_list = child->child_list;

	child_list = g_slist_next(child_list);
	child = child_list->data;
	if(child->type != C_NEWLINE) {
		return;
	}

	child_list = g_slist_next(child_list);
	child = child_list->data;
	if (child->type != C_FORM_BUTTON) {
		return;
	}

	if(child->child_list != NULL) {
		child = child->child_list->data;
		if (child->type != C_BUTTON) {
			return;
		}
	}

	/* Now check the "grandchildren": the elements of the group. */
	do {
		group_child = group_child_list->data;
		if((group_child->type != C_LABEL) &&
		   (group_child->type != C_NEWLINE)) {
			return;
		}
	} while(group_child_list = g_slist_next(group_child_list));

	/* If we reach this point, we have a help window; hint the
	 * whole thing to expand and be wide.
	 */
	hint = EXPAND | WIDE;
	hint_tree(w, &hint);
}

void
read_string(gpointer data, gint fd, GdkInputCondition condition)
{
	char *buffer;
	widget_info *w;

	while (io_data_ready()) {
		buffer = io_get_line();
		if(buffer != NULL) {
			// g_print("%s\n", buffer);
			w = (widget_info*) parse_line (buffer);
			if (w && (w->type == C_MAINFORM)) {
				hint_form(w);
				create_mainform(w);
			}
		} else {
			gdk_input_remove(input_tag);
			/* 
			 * #if 0 for testing, #if 1 for real use
			 */
#if 1
			close(io->in);  /* tell any pipe-connected program what's up */
			close(io->out);
			gtk_main_quit();
#endif
		}
	}
}

void
log_handler(const gchar *log_domain, GLogLevelFlags log_level,
	    const gchar *message, gpointer data)
{
	GtkWidget *dialog, *label;
	dialog = gnome_dialog_new(log_domain ? log_domain : (gchar*)data,
				  GNOME_STOCK_BUTTON_CLOSE,
				  NULL);
	if(app != NULL) {
		gtk_window_set_transient_for(GTK_WINDOW(dialog),
					     GTK_WINDOW(app));
	}
	label = gtk_label_new(message);
	gtk_box_pack_start_defaults(GTK_BOX((GNOME_DIALOG(dialog))->vbox),
				    label);
	gtk_widget_show(label);
	gnome_dialog_run_and_close(GNOME_DIALOG(dialog));
}

int
main(int argc, char **argv)
{
	callbacks c;
	struct sigaction stopaction;

	/* linuxconf seems to go haywire if we suspend, don't know why,
	 * hack around it for now
	 */
	stopaction.sa_handler = SIG_IGN;
	sigaction(SIGTSTP, &stopaction, NULL);

	io = malloc(sizeof(ios));
	if (!io) {
		perror("allocating ios");
		exit(1);
	}

	if (!io_init(io, &argc, &argv, P_treemenu)) {
		return 1;
	}

#ifdef GTK_HAVE_FEATURES_1_1_4
	gnome_init ("linuxconf", NULL, argc, argv);
#else
	gnome_init ("linuxconf", NULL, argc, argv, ARGP_NO_ERRS, NULL);
#endif

	showing_tabs = g_hash_table_new(g_str_hash, g_str_equal);

	c.commandAlloc = &commandAlloc;
	c.commandFree = NULL;
	c.commandDeleteTree = &commandDeleteTree;
	c.protocolCheck = &protocolCheck;
	c.writeData = &io_write_data;
	c.dumpTree = (dump_tree) &widget_dump_tree;
	parse_init(c);

	g_log_set_handler(G_LOG_DOMAIN, G_LOG_LEVEL_ERROR, log_handler,
			  _("Error"));
	g_log_set_handler(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING, log_handler,
			  _("Warning"));
	g_log_set_handler(G_LOG_DOMAIN, G_LOG_LEVEL_MESSAGE, log_handler,
			  _("Message"));
	g_log_set_handler(G_LOG_DOMAIN, G_LOG_LEVEL_INFO, log_handler,
			  _("Information"));
	g_log_set_handler(G_LOG_DOMAIN, G_LOG_LEVEL_CRITICAL, log_handler,
			  _("Critical"));
	g_log_set_handler(G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, log_handler,
			  _("Debugging Information"));

	input_tag = gdk_input_add (io->in, GDK_INPUT_READ,
				   read_string, NULL);
	gtk_main ();
	return 0;
}
