/*
 * A GNOME front end for LinuxConf
 *
 * parse.c: parse the incoming stream into data structures that
 * can completely represent an abstract form.  These data
 * structures can be rendered at will by an arbitrary display
 * engine.  The GNOME engine uses gtk for the display engine.
 *
 * Copyright (C) 1997 the Free Software Foundation
 * Copyright (C) 1998 Red Hat Software, Inc.
 *
 * Authors: Miguel de Icaza (miguel@gnu.org)
 *          Michael K. Johnson <johnsonm@redhat.com>
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <glib.h>

#include "parse.h"

static command_alloc commandAlloc;
static command_delete_tree commandDeleteTree;
static command_free commandFree;
static protocol_check protocolCheck;
static write_data writeData;
static dump_tree dumpTree;

static int form_level = 0;
static command *current_command, *current_form;

/* string accumulator */
struct reg_s {
	char **str;
	int size;
	int next;
} reg;

/* id->command hash */
static GHashTable *hash;

#define bind_sym(x,y) g_hash_table_insert (hash, g_strdup (x), y);
#define unbind_sym(x) g_hash_table_remove (hash, x);

static char **
split_string (char *str, int items)
{
	char **result;
	int  i;

	result = g_malloc0 (sizeof (char *) * (items + 1));

	for (i = 0; i < items && *str; i++){
		result [i] = str;

		if (*str == '"') {
			/* handle quoted string with embedded spaces
			 * and escaped quotes */

			result [i] = ++str;
			while (*str && *str != '"')  {
				if (*str == '\\')
					if (*(++str) == '"' || (*str) == '\\')
						/* remove the \ character */
						/* g_memove isn't overlap-safe */
						memmove(str-1, str, strlen(str));
					else
						break;
				str++;
			}

		} else {
			/* spaces demarcate substrings */

			while (*str && (!isspace (*str)))
				str++;
		}

		if (*str) {
			*str = 0;
			str++;
		}

		while (isspace (*str))
			str++;

	}
	result [i] = str;
	return result;
}


static void
unrtf (char * buf) {
	char * buf2 = alloca(strlen(buf) + 1);
	char * dest = buf2;
	char * src = buf;

	while (*src) {
		switch (*src) {
		  case '\b': *(dest - 1) = *(++src), src++; break;
		  default: *dest++ = *src++;
		}
	}

	*dest = '\0';

	strcpy(buf, buf2);
}

/* fill_in_command_info MUST be called for every command structure.
 * parent is NULL for toplevels.
 * data is optional.
 */
static void
fill_in_command_info(command *parent, char *this_id, command_type type)
{
	command *c, *p = parent;

	if (commandAlloc)
		c = (*commandAlloc) ();
	else
		c = g_malloc0(sizeof(command));
	current_command = c;
	c->type = type;
	c->parent = parent;
	c->id = g_strdup(this_id);
	bind_sym (c->id, c);

	/* now manage command hierarchy info */
	if (p) {
	    /* is a child of another command */
	    if (p->path) {
		c->path = g_malloc(strlen(p->path) + strlen(p->id) + 2);
		sprintf(c->path, "%s.%s", p->path, p->id);
	    } else {
		c->path = g_strdup(p->id);
	    }
	    p->child_list = g_slist_append(p->child_list, c);
	    c->form = p->form;
	} else {
	    /* mainform only */
	    current_form = c;
	    form_level = 0;
	    c->form = c;
	}
}

static void
set_form(void) {
	form_level++;
	current_form = current_command;
	if (current_command->parent)
		current_command->form = current_command->parent->form;
	/* if not current_command->parent then form was set in
	 * fill_in_command_info function
	 */
}

static void
free_ci (combo_item *ci) {
	if (!ci) return;

	if (ci->value) g_free(ci->value);
	if (ci->comment) g_free(ci->comment);
	g_free (ci);
}

static void
free_cli (clist_item *ci) {
	if (!ci) return;

	if (ci->id) g_free (ci->id);
	if (ci->values) g_free (ci->values);
	g_free (ci);
}



static void
create_form (char *str)
{
	fill_in_command_info(current_form, str, C_FORM);
	set_form();
}

static void
create_main_form (char *str)
{
	char **strs;

	strs = split_string (str, 2);
	fill_in_command_info(NULL, strs [0], C_MAINFORM);
	set_form();
	current_command->str = g_strdup(strs[1]);
	current_command->data = g_malloc(sizeof(mainform_data));
	free(strs);
}

static void
create_form_button (char *str)
{
	fill_in_command_info(current_form, str, C_FORM_BUTTON);
	set_form();
}
	
static void
create_book (char *str)
{
	fill_in_command_info(current_form, str, C_BOOK);
	set_form();
}

static void
create_button (char *str)
{
	char **strs;
	command_type ct;

	strs = split_string (str, 3);
	if (strs[1][0] == '1')
		ct = C_BUTTON_DUMP;
	else
		ct = C_BUTTON;
	fill_in_command_info(current_form, strs [0], ct);
	current_command->str = g_strdup(strs[2]);
	free(strs);
}

static void
create_button_fill (char *str)
{
	char **strs;

	strs = split_string (str, 2);
	fill_in_command_info(current_form, strs [0], C_BUTTON_FILL);
	current_command->str = g_strdup(strs[1]);
	free(strs);
}

static void
create_button_xpm (char *str)
{
	char **strs;

	strs = split_string (str, 3);
	fill_in_command_info(current_form, strs [0], C_BUTTON_XPM);
	/* chance the xpm ID being reused (Doh!) so that we can cache
	 * the graphics in the graphics driver.
	 */
	current_command->data = g_hash_table_lookup(hash, strs[1]);
	if (strlen(strs[2]) > 1 || strs[2][0] != '-')
		current_command->str = g_strdup(strs[2]);
	free(strs);
}

static void
create_button_xpmf (char *str)
{
	char **strs;

	strs = split_string (str, 3);
	fill_in_command_info(current_form, strs [0], C_BUTTON_XPMF);
	/* use data for the filename; this command should rarely be used,
	 * so I do not feel like allocating it extra space in the command. */
	current_command->data = g_strdup(strs[1]);
	if (strlen(strs[2]) > 1 || strs[2][0] != '-')
		current_command->str = g_strdup(strs[2]);
	free(strs);
}

static void
create_combo (char *str)
{
	char **strs;

	strs = split_string (str, 3);
	fill_in_command_info(current_form, strs [0], C_COMBO);
	current_command->idata = atoi(strs[1]);
	current_command->str = g_strdup(strs[2]);
	/* current_command->data will be used for combo item GSList */
	free(strs);
}

static void
create_combo_item (char *str)
{
	char **strs;
	command *combo;
	combo_item *ci;

	strs = split_string (str, 3);
	combo = g_hash_table_lookup(hash, strs[0]);
	if (combo->type != C_COMBO) return;
	ci = g_malloc0(sizeof(combo_item));
	ci->value = g_strdup(strs[1]);
	if (strs[2][0] != '\0')
		ci->comment = g_strdup(strs[2]);
	combo->data = g_slist_append((GSList *)combo->data, ci);
	free (strs);
}

static void
create_choice (char *str)
{
	char **strs;
	
	strs = split_string (str, 2);
	fill_in_command_info(current_form, strs [0], C_CHOICE);
	current_command->str = g_strdup(strs [1]);
	free (strs);
}

static void
choice_item (char *str)
{
	char **strs;
	command *choice;
	combo_item *ci;

	strs = split_string (str, 3);
	choice = g_hash_table_lookup(hash, strs[0]);
	if (choice->type != C_CHOICE) return;
	ci = g_malloc0(sizeof(combo_item));
	if (strs[1][0] == '\0' ||
	    (strs[1][0] == ' ' && strs[1][1] == '\0'))
		/* empty value implies comment is value */
		ci->value = g_strdup(strs[2]);
	else
		ci->value = g_strdup(strs[1]);
	ci->comment = g_strdup(strs[2]);
	choice->data = g_slist_append((GSList *)choice->data, ci);
	free (strs);
}

static void
create_clist (char *str)
{
	char **strs;
	char **cols;
	int numcols, i;
	clist_data *cd;
	
	strs = split_string (str, 2);
	fill_in_command_info(current_form, strs [0], C_CLIST);
	numcols = current_command->idata = atoi(strs [1]);
	cd = g_malloc0 (sizeof (clist_data));
	cols = split_string (strs[2], numcols);

	cd->titles = g_malloc0((numcols + 1) * sizeof(char *));
	for (i = 0; i < numcols; i++) {
		cd->titles[i] = g_strdup (cols[i]);
	}

	current_command->data = cd;

	free (cols);
	free (strs);
}

static void
create_clist_item (char *str)
{
	char **strs;
	char **cols;
	int numcols, i;
	command *clist;
	clist_item *ci;
	clist_data *cd;

	strs = split_string (str, 2);
	clist = g_hash_table_lookup(hash, strs[0]);
	if (!clist) exit(1); /* corrupt datastream can't be trusted */
	if (clist->type != C_CLIST) return;

	cd = clist->data;

	numcols = clist->idata;
	ci = g_malloc0 (sizeof (ci));
	ci->id = g_strdup (strs[1]);
	cols = split_string (strs[2], numcols);

	ci->values = g_malloc0((numcols + 1) * sizeof(char *));
	for (i = 0; i < numcols; i++) {
		ci->values[i] = g_strdup (cols[i]);
	}

	cd->itemlist = g_slist_append(cd->itemlist, ci);

	free (cols);
	free (strs);
}

static void
create_checkbox (char *str)
{
	char **strs;

	strs = split_string (str, 3);
	fill_in_command_info(current_form, strs [0], C_CHECKBOX);
	if (strs[1][0] == '1')
		current_command->idata = CB_ON;
	else
		current_command->idata = CB_OFF;
	current_command->str = g_strdup(strs[2]);
	free (strs);
}

static void
create_radio (char *str)
{
	command *radiogroup;
	char **strs;

	strs = split_string (str, 4);
	radiogroup = g_hash_table_lookup (hash, strs [0]);
	if (radiogroup) {
		if (radiogroup->type != C_RADIO) return;
		fill_in_command_info(current_form, "", C_RADIO);
	} else {
		fill_in_command_info(current_form, strs [0], C_RADIO);
		radiogroup = current_command;
	}
	current_command->ref = radiogroup;
	current_command->data = g_strdup(strs[1]); /* value */
	if (strs [2][0] == '1')
		current_command->idata = CB_ON;
	else
		current_command->idata = CB_OFF;
	current_command->str  = g_strdup(strs[3]); /* display string */

	free (strs);
}

static void
create_group (char *str)
{
	char **strs;

	strs = split_string (str, 2);
	fill_in_command_info(current_form, strs [0], C_GROUP);
	if (strs [1][0])
		current_command->str = g_strdup(strs [1]);
	set_form();
	free (strs);
}

static void
create_label (char *str)
{
	char **strs;
	
	strs = split_string (str, 1);
	fill_in_command_info(current_form, "", C_LABEL);
	current_command->str = g_strdup(strs[0]);
	unrtf(current_command->str);
	free (strs);
}

static void
create_page (char *str)
{
	char **strs;

	if (current_form->type != C_BOOK)
		return;

	strs = split_string (str, 2);
	fill_in_command_info(current_form, strs [0], C_PAGE);
	current_command->str = g_strdup (strs [1]);
	set_form();
	free (strs);
}


static void
create_treemenu (char *str)
{
	char **strs;
	tree_item_data *item;

	strs = split_string (str, 1);
	fill_in_command_info(current_form, strs [0], C_TREEMENU);
	item = g_malloc0(sizeof(tree_item_data));
	item->numchildren = 0;
	current_command->data = item;
	set_form();
	free (strs);
}

static void
create_treesub (char *str)
{
	char **strs;
	tree_item_data *item, *parentitem;

	if (current_form->type != C_TREEMENU &&
	    current_form->type != C_TREESUB)
		return;

	item = g_malloc0(sizeof(tree_item_data));

	strs = split_string (str, 3);
	fill_in_command_info(current_form, current_form->id, C_TREESUB);
	current_command->str = g_strdup (strs [2]);
	current_command->data = item;
	item->icon_xpm = g_hash_table_lookup(hash, strs[1]);
	item->expand = strcmp("0", strs[0]);
	parentitem = current_form->data;
	item->num = parentitem->numchildren;
	parentitem->numchildren++;

	set_form();
	free (strs);
}

static void
create_treeitem (char *str)
{
	char **strs;
	tree_item_data *item, *parentitem;

	if (current_form->type != C_TREEMENU &&
	    current_form->type != C_TREESUB)
		return;

	strs = split_string (str, 2);
	item = g_malloc0(sizeof(tree_item_data));

	fill_in_command_info(current_form, current_form->id, C_TREEITEM);
	current_command->data = item;
	current_command->str = g_strdup (strs [1]);
	/* chance the xpm ID being reused (Doh!) so that we can cache
	 * the graphics in the graphics driver.
	 */
	item->icon_xpm = g_hash_table_lookup(hash, strs[0]);
	parentitem = current_form->data;
	item->num = parentitem->numchildren;
	parentitem->numchildren++;

	free (strs);
}

static void
get_tree_path(command *c, char *buf, int *buflen)
{
	static char numbuf[16];
	static tree_item_data *item;
	static int len;
	static int slen;

	/* terminate */
	if (!c->parent ||
	    (c->parent->type != C_TREEITEM &&
	     c->parent->type != C_TREESUB  &&
	     c->parent->type != C_TREEMENU)) {
		/* buffer should be empty */
		return;
	}

	/* recurse */
	get_tree_path(c->parent, buf, buflen);

	/* progress */
	item = c->data;
	sprintf(numbuf, "%d/", item->num);
	len = strlen(buf);
	slen = strlen(numbuf);
	if (len+slen >= *buflen) {
		*buflen += 64;
		g_realloc(buf, *buflen);
	}
	strcat(buf, numbuf);
}

char*
parse_get_tree_path(command *c)
{
	char *buf;
	int buflen = 64;

	buf = g_malloc0(buflen);
	get_tree_path(c, buf, &buflen);
	return buf;
}



static void
create_entry (char *str, int visible)
{
	char **strs;
	
	strs = split_string (str, 3);
	fill_in_command_info(current_form, strs [0],
			     visible ? C_STRING : C_PASSWORD);
	/* idata is length of entry in characters */
	current_command->idata = atoi (strs [1]);
	current_command->str = g_strdup(strs [2]);
	free (strs);
}

static void
create_string (char *str)
{
	create_entry(str, 1);
}

static void
create_password (char *str)
{
	create_entry(str, 0);
}

static void
create_hline (char *str)
{
	char **strs;
	
	strs = split_string (str, 1);
	fill_in_command_info(current_form, "", C_HLINE);
	if (strs [0][0])
		current_command->str = g_strdup(strs[0]);
	free (strs);
}

static void
newline (char *str)
{
	fill_in_command_info(current_form, "", C_NEWLINE);
}

static void
skip (char *str)
{
	fill_in_command_info(current_form, "", C_SKIP);
	current_command->idata = atoi(str);
}

static void
end (char *str)
{
	if (! form_level)
		return;

	form_level--;
	current_command = current_form;
	if (current_form->parent)
		current_form = current_form->parent;
}

/*
 * When we receive this command, it means that we have to change the layout of
 * the last inserted widget.
 * This also provides alignment contraints.
 */
static void
dispolast (char *str)
{
	char **strs;
	disposition *disp;

	/* FIXME: unimplemented elements can cause dispolast to apply to
	 * the WRONG element.  They can also cause dispolast to apply to
	 * non-existing elements.
	 */
	if (! current_command)
		return;
	
	strs = split_string (str, 4);
	disp = g_malloc0(sizeof(disposition));

	switch (strs[0][0])
	{
	    case 'l':
		disp->horiz = H_LEFT;
		break;
	    case 'r':
		disp->horiz = H_RIGHT;
		break;
	    case 'c':
	    default:
		disp->horiz = H_CENTER;
	}

	switch (strs[2][0])
	{
	    case 't':
		disp->vert = V_TOP;
		break;
	    case 'b':
		disp->vert = V_BOTTOM;
		break;
	    case 'c':
	    default:
		disp->vert = V_CENTER;
	}

	disp->num_horiz = atoi (strs[1]);
	disp->num_vert  = atoi (strs[3]);

	current_command->disp = disp;
	free (strs);
}

static void
dump (char *str)
{
	command *form;

	form = g_hash_table_lookup (hash, str);
	if (!form) {
		return;
	}
	(*dumpTree) (form);
}

/* delete_tree deletes a command tree */
static void
delete_tree (command *c)
{

	g_slist_foreach (c->child_list, (GFunc) delete_tree, NULL);
	g_slist_free (c->child_list);

	unbind_sym(c->id);

	g_free (c->id);
	if (c->disp)
		g_free (c->disp);
	if (c->str)
		g_free (c->str);
	if (c->data)
		switch (c->type) {
		    case C_COMBO:
		    case C_CHOICE:
			g_slist_foreach(c->data, (GFunc) free_ci, NULL);
			g_slist_free(c->data);
			break;
		    case C_CLIST:
			do {
			    clist_data *cd = c->data;

			    g_slist_foreach (cd->itemlist,
					     (GFunc) free_cli, NULL);
			    g_slist_free (cd->itemlist);
			    g_free (cd->titles);
			    g_free (cd);
			} while (0);
			break;
		    case C_BUTTON_XPMF:
		    case C_RADIO:
		    case C_MAINFORM:
		    case C_TREEMENU:
		    case C_TREEITEM:
		    case C_TREESUB:
			g_free (c->data);
			break;
		    default:
			break;
		}
	if (c->path)
		g_free (c->path);
	/* c->ref, if it exists, is always a ref that should not be freed */

	if (commandFree)
		(*commandFree) (c);
	else
		g_free(c);
}

static void
delete (char *str)
{
	command *del;

	del = g_hash_table_lookup (hash, str);
	if (!del)
		return;

	if (commandDeleteTree)
		commandDeleteTree(del);

	/* destroys the whole tree */
	delete_tree (del);
}

static void
unimplemented (char *str)
{
}

static void
str (char *str)
{
	/* we optimize in the case of transferring an XPM because
	 * split_string is rather expensive by contrast
	 */

	/* assume we are getting part of an xpm: we only want data */
	if (str[0] != '"' || str[2] != '"') return;

	/* now we know we have a data line, cut it off at the quotes */
	str += 3;
	*(strrchr(str, '\\')) = '\0';

	if (!reg.str) {
		reg.size = 0;
		reg.next = 0;
	}

	if (reg.next >= reg.size) {
		reg.size += 128;
		reg.str = realloc(reg.str, (reg.size * sizeof(*reg.str)));
	}

	reg.str[reg.next++] = g_strdup (str);
	reg.str[reg.next] = NULL;
}

static void
xfer_xpm (char *str)
{
	bind_sym (str, reg.str);
	reg.str = NULL;
}

static void
sidetitle (char *str)
{
	/* nothing */
}

static void
check_version (char *str)
{
	char **strs;
	int i, e;

	strs = split_string (str, 2);
	i = atoi (strs [0]);
	e = (*protocolCheck) (i, i == PROTOCOL_VERSION, strs [1]);
	if (i != PROTOCOL_VERSION || e != 1) {
		/* tell the other end about the problem */
		(*writeData) ("action badver 1");
	}

	free(strs);
}

static void
handle_enteraction (char *str)
{
	char **strs;
	mainform_data *md;

	if (! current_form || !current_form->form)
		return;
	strs = split_string (str, 1);
	md = current_form->form->data;
	md->enteraction = g_strdup(strs[0]);
	free(strs);
}

static void
handle_curfield (char *str)
{
	char **strs;
	command *c;
	mainform_data *md;

	strs = split_string (str, 2);
	c = g_hash_table_lookup (hash, strs[1]);
	if (!c || !c->form || !c->form->data) return;

	md = c->form->data;
	md->focus = c;
	
	free(strs);
}

static struct {
	char *name;
	void (*cmd)(char *str);
} cmds [] = {
	{ "Book",        create_book },
	{ "Button",      create_button },
	{ "ButtonFill",  create_button_fill },
	{ "Checkbox",    create_checkbox },
	{ "Choice",      create_choice },
	{ "Choice_item", choice_item }, 
	{ "Clist",       create_clist },
	{ "Clist_item",  create_clist_item },
	{ "Combo",       create_combo },
	{ "Combo_item",  create_combo_item },
	{ "Curfield",    handle_curfield },
	{ "Delete",      delete },
	{ "Dispolast",   dispolast },
	{ "Dump",        dump },
	{ "End",         end },
	{ "Enteraction", handle_enteraction },
	{ "Form",        create_form },
	{ "FormButton",  create_form_button },
	{ "Group",       create_group },
	{ "Groupfit",    create_group },
	{ "Hline",       create_hline },
	{ "Label",       create_label },
	{ "MainForm",    create_form  },
	{ "Newline",     newline },
	{ "Page",        create_page },
	{ "Password",	 create_password },
	{ "Radio",	 create_radio },
	{ "Sidetitle",   sidetitle },
	{ "Skip",        skip },
	{ "Str",         str },
	{ "String",      create_string },
	{ "Treeelem",    create_treeitem },
	{ "Treemenu",    create_treemenu },
	{ "Treesub",     create_treesub },
	{ "Version",	 check_version },
	{ "Xfer_xpm",    xfer_xpm },
	{ NULL, NULL }
};

static struct {
	void (*cmd)(char *str);
} cmds_bynumber [] = {
	{ unimplemented },	/* 0. indexing starts at 1 */
	{ str },		/* 1 str */
	{ newline },		/* 2 */
	{ skip },		/* 3 */
	{ create_hline },	/* 4 */
	{ dispolast },		/* 5 */
        { create_label },	/* 6 */
	{ create_string },	/* 7 */
	{ create_checkbox },	/* 8 */
	{ create_radio },	/* 9 radio */
	{ create_button },	/* 10 */
	{ create_button_xpm },	/* 11 button xpm */
	{ create_button_xpmf },	/* 12 button xpmf */
	{ create_choice },	/* 13 */
	{ choice_item },	/* 14 */
	{ unimplemented },	/* 15 list */
	{ unimplemented },	/* 16 list item */
	{ create_combo },	/* 17 */
	{ create_combo_item },	/* 18 */
	{ create_group },	/* 19 */
	{ create_form },	/* 20 */
	{ create_page },	/* 21 */
	{ create_book },	/* 22 */
	{ create_main_form },	/* 23 */
	{ end },		/* 24 */
	{ delete },		/* 25 */
	{ dump },		/* 26 */
	{ unimplemented },	/* 27 icon xpm */
	{ xfer_xpm },		/* 28 xfer xpm */
	{ sidetitle },		/* 29 sidetitle */
	{ unimplemented },	/* 30 fill */
	{ create_form_button },	/* 31 formbutton */
	{ create_group },	/* 32 groupfit */
	{ unimplemented },	/* 33 setweightlast */
	{ create_label },	/* 34 FIXME: richtext with text or xm-html */
	{ create_password },	/* 35 password */
	{ create_button_fill },	/* 36 buttonfill */
	{ handle_enteraction },	/* 37 enteraction */
	{ handle_curfield },	/* 38 curfield */
	{ check_version },	/* 39 version */
	{ create_treemenu },	/* 40 treemenu */
	{ create_treeitem },	/* 41 treeitem */
	{ create_clist },	/* 42 */
	{ create_clist_item },	/* 43 */
	{ unimplemented },	/* 44 html */
	{ create_treesub },	/* 45 treesub */
	{ unimplemented }	/* 46 last */
};
	
static void
process (char *str)
{
	char *space;
	int i;
		
	while (*str && (*str == ' ' || *str == '\t'))
		str++;

	space = strchr (str, ' ');
	if (space)
		*space = 0;

	if (isdigit (str [0])){
		i = atoi (str);
		(*cmds_bynumber [i].cmd) (space+1);
	} else 
		for (i = 0; cmds [i].name; i++){
			if (strcmp (cmds [i].name, str))
				continue;
			(*cmds [i].cmd) (space+1);
		}
}


int
parse_init(callbacks cb) {

	reg.str = NULL;

	commandAlloc = cb.commandAlloc;
	commandDeleteTree = cb.commandDeleteTree;
	commandFree = cb.commandFree;
	protocolCheck = cb.protocolCheck;
	writeData = cb.writeData;
	dumpTree = cb.dumpTree;
	current_form = NULL;

	hash = g_hash_table_new (g_str_hash, g_str_equal);

	return 1;
}

/* parse_line() takes 1 char * argument, the current line.
 * if the line is the end of a top-level frame, parse_line() returns
 * a valid command which should be displayed.  Otherwise it returns
 * NULL.
 */
command *
parse_line(char *line) {
	int return_candidate = 0;

	if (form_level)
		return_candidate = 1;
	if (line[0] == '\0') return NULL;
	process(line);
	/* fprintf (stderr, "%d, %p\n", form_level, current_form); */
	/* fflush(stderr); */
	if (return_candidate && !form_level)
		return current_form;
	else
		return NULL;
}

