/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

DUMPER(page)
{
}

HANDLER(page)
{
	GtkWidget *widget = NULL, *label1 = NULL, *label2 = NULL;

	char *id = (char*)g_list_nth_data(args, 0);
	char *title = (char*)g_list_nth_data(args, 1);
	g_return_val_if_fail(id != NULL, NULL);
	g_return_val_if_fail(title != NULL, NULL);

	widget = gtk_vbox_new(FALSE, 0);

	title = g_strdup(title);
	label1 = gtk_label_new(title);
	gtk_signal_connect(GTK_OBJECT(label1), "destroy",
			   GTK_SIGNAL_FUNC(free_data), title);

	title = g_strdup(title);
	label2 = gtk_label_new(title);
	gtk_signal_connect(GTK_OBJECT(label2), "destroy",
			   GTK_SIGNAL_FUNC(free_data), title);

	gtk_notebook_append_page_menu(GTK_NOTEBOOK(previous_notebook),
				      widget, label1, label2);
	parents_push(widget);

	return widget;
}
