/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"

DUMPER(label)
{
}

HANDLER(label)
{
	GtkWidget *widget = NULL;

	char *title = (char*)g_list_nth_data(args, 0);
	g_return_val_if_fail(title != NULL, NULL);

	title = g_strdup(strlen(title) ? title : " ");
	widget = gtk_label_new(title);
	gtk_signal_connect(GTK_OBJECT(widget), "destroy",
			   GTK_SIGNAL_FUNC(free_data), title);

	gtk_misc_set_alignment(GTK_MISC(widget), 0, 0.5);
	gtk_label_set_line_wrap(GTK_LABEL(widget), FALSE);

	gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_VFLAGS,
			    GINT_TO_POINTER(GTK_SHRINK));
	gtk_object_set_data(GTK_OBJECT(widget), GNOME_LINUXCONF_HINT_VPAD,
			    GINT_TO_POINTER(-1));

	parent_insert(NULL, widget);

	return NULL;
}
