/*
 * A GTK+/GNOME front end for LinuxConf.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * Author: Nalin Dahyabhai <nalin@redhat.com>
 *
 */

#include "gnome-linuxconf.h"
HANDLER(delete)
{
	GtkWidget *widget = NULL;
	char *id = (char*)g_list_nth_data(args, 0);

	g_return_val_if_fail(id != NULL, NULL);

	widget = get_widget_by_name(id);
	g_return_val_if_fail(GTK_IS_WIDGET(widget), NULL);
	gtk_widget_destroy(widget);

	return NULL;
}
